var http = require('http');
var fs= require('fs')
//create a server object:
http.createServer(function (req, res) {
  var url = req.url;

  if(url ==='/barleytea.js'){
    res.setHeader('Content-Type', 'application/javascript');
    res.write(fs.readFileSync('./barleytea.js','utf8')); //write a response
    res.end(); //end the response
  } else if(url ==='/distroMaker'){
    res.setHeader('Content-Type','text/html')
    res.write(fs.readFileSync('./distroMaker.html','utf8')); //write a response
    res.end(); //end the response
  }else{
    console.log(url)
    try {
      res.write(fs.readFileSync('.'+url.split('?')[0],'utf8')); //write a response
      res.end(); //end the response
    } catch(e) {
      res.statusCode=404
      res.end();
    }
    
  }

  
}).listen(3000, function(){
 console.log("server start at port 3000. Check it out at http://localhost:3000/public/distroMaker.html"); //the server object listens on port 3000
});