/*
ISC License

Copyright (c) 2020 Andrew Ulrich

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/**********Templating************/
const templateCache=new WeakMap()

const diffObj = {
  currentID:0,
  idPrefix:'barleytea-UID',
  generateUID() {
    return this.idPrefix + this.currentID++;
  },
  convertSelfClosing(htmlString) {
  //this function comes from https://github.com/WebReflection/uparser/blob/master/index.js
    const selfClosing = /<([a-z]+[a-z0-9:\._-]*)([^>]*?)(\/>)/ig;
    const trimEnd = /\s+$/;
    const empty = /^(?:area|base|br|col|embed|hr|img|input|keygen|link|menuitem|meta|param|source|track|wbr)$/i;

    function regular(original, name, extra) {
    //regularize a self-closing tag
      return empty.test(name) ? original : "<".concat(name).concat(extra.replace(trimEnd, ''), "></").concat(name, ">");
    }
    return htmlString.replace(selfClosing, regular);
  },
  identifyKeyChanges(rawStrings,keys,parent) {
    //note: does not match custom elements with non-ascii characters
    const firstElementSearch=/^\s*(<[a-zA-Z][a-zA-Z0-9:\._-]*[^\s>]*)/;
    const firstElementMatch=rawStrings[0].match(firstElementSearch);
    let firstString='';
    if(firstElementMatch && firstElementMatch[1])
      firstString = rawStrings[0].replace(firstElementMatch[1],firstElementMatch[1]+' data-first-element="" ');
    else
      throw new Error('template must have a root element: '+rawStrings.join(''));

    const strings=[firstString].concat(rawStrings.slice(1));
    //can be: updateAttr,updateProp,replaceRange
    //updateProp is how you also attach listeners. Does not work for custom events. See https://developer.mozilla.org/en-US/docs/Web/Guide/Events/Event_handlers
    //if replaceRange, keyType describes what the tagged teplate literal key is. It can be a keyedTagArray, tagObject, or it simply a textNode (which is the default)
    const attrSearch=/<[-a-zA-Z][^>]*\s+([^\s\\>"'=]+)\s*=\s*(['"]?)$/m;
    const replace=/([^\s\\>"'=]+)\s*=\s*(['"]?)$/m;
    
    let changeInfo=[];
    let soFar='';
    if(keys) {
      for(let i=0;i<keys.length;i++) {
        soFar+=strings[i];
        const attrMatch=soFar.match(attrSearch);
        if(attrMatch) {
          //attrMatch[1] is the attribute name
          //attrMatch[2] if it exists, is the opening quote
          //create a temporary data attribute we can use as a temporary selector once the element is created. Just don't forget to remove it and save off a reference to the element.
          soFar=soFar.replace(replace,`data-template-key-${i}=""`) ;

          //cleaning up the extra quote if it exists
          if(strings[i+1][0]=='"') strings[i+1]=strings[i+1].replace('"','') ;

          const changeType=attrMatch[1].indexOf('.')==0 ? 'updateProp' : 'updateAttr';

          changeInfo.push({
            changeType,
            name:attrMatch[1],
            parent
          });
        } else {
          //it's replaceRange
          //replace the holes with a placeholder span with data-replace="${i}"` 
          soFar+=`<template data-replace="${i}"></template>`;
          //later, replace the placeholder with two book-end comment nodes which we keep the references to
          changeInfo.push({
          changeType:'replaceRange',
          parent
          });
        }
      } // end for loop
    }

    const template=document.createElement('template')
    //add the last string
    soFar+=strings[strings.length-1];
    template.innerHTML=this.convertSelfClosing(soFar);
    const rootEl=template.content.querySelector('[data-first-element=""]');
    //cleanup to prevent confusion
    rootEl.removeAttribute('data-first-element');

    if(rootEl.previousElementSibling || rootEl.nextElementSibling)
      throw new Error('template must have a single root element: '+soFar);

    //identify the type of change each key needs and get the necessary info to change it
    if(keys) {
      for(let i=0; i<keys.length; i++) {
        if(changeInfo[i].changeType=='replaceRange') {
          //set the start and end nodes
          const toReplace=rootEl.querySelector(`[data-replace="${i}"]`);
          if(toReplace) {
            const startNode=document.createElement('template');
            startNode.setAttribute('id',this.generateUID())
            const endNode=document.createElement('template');
            endNode.setAttribute('id',this.generateUID())
            toReplace.parentNode.insertBefore(startNode,toReplace)
            toReplace.replaceWith(endNode)
            //get start and end node ids so they can be used later
            changeInfo[i].startNodeID=startNode.getAttribute('id')
            changeInfo[i].endNodeID=endNode.getAttribute('id')

            changeInfo[i].keyType=Array.isArray(keys[i]) ? 'keyedTagArray' 
               : (keys[i] && keys[i] instanceof HTMLElement ? 'htmlElement'
              : (keys[i] && keys[i].constructor==Object && keys[i].strings && keys[i].keys ? 'tagObject'
              : ''));
            if(changeInfo[i].keyType == 'keyedTagArray' 
              && keys[i].some(obj=>!obj || !obj.keys || obj.key===undefined || !obj.strings))
                throw new Error('Array input must be an array of objects of the form {strings,keys,key}. You can use the "keyed" function, e.g. "keyed(yourKey)`<your-template />`');
          } else {
            //toReplace will only exist as text content within textareas
            const toFind=`<template data-replace="${i}"></template>`
            const textAreas=Array.from(rootEl.querySelectorAll('textarea'));
            if(rootEl.tagName=='TEXTAREA') textAreas.push(rootEl)
            const toUpdate = textAreas.find(ta=>ta.textContent.includes(toFind))
            if(toUpdate==undefined) throw new Error('Error buiding template. Please remember that partial string attributes are not supported- this is a common cause of this error.')
            if(!toUpdate.hasAttribute('id')) toUpdate.setAttribute('id',this.generateUID());
            changeInfo[i].toUpdateID=toUpdate.getAttribute('id')
            changeInfo[i].templateText=toUpdate.textContent
            changeInfo[i].toFind=toFind
            changeInfo[i].keyType='textarea'
          }       
        } else {
          const toUpdate=template.content.querySelector(`[data-template-key-${i}=""]`)

          if(!toUpdate.hasAttribute('id')) toUpdate.setAttribute('id',this.generateUID());
          //cleanup to prevent confusion
          toUpdate.removeAttribute(`data-template-key-${i}`)
          changeInfo[i].toUpdateID=toUpdate.getAttribute('id')
        }
      }
    }

    return {rootEl,changeInfo,strings:rawStrings};
  },

  updateAttr(changeInfo,newVal,toUpdate) {
    //get the element, diff the attribute value, update the attribute
    //changeInfo= {type: 'attributeChange', name, toUpdate}
    // name is the attribute name
    // const toUpdate=document.getElementById(changeInfo.toUpdateID);
    if(''+newVal !== toUpdate.getAttribute(changeInfo.name))
      toUpdate.setAttribute(changeInfo.name,newVal);

    //this is in case the ID has been updated during this call of applyChanges
    if(changeInfo.toUpdateID !== toUpdate.getAttribute('id'))
      changeInfo.toUpdateID=toUpdate.getAttribute('id');
  },
  updateProp(changeInfo,newVal,toUpdate) {
    //changeInfo= {type: 'attributeChange', name, toUpdate}
    //name is the prop name, preceded by a .
    // const toUpdate=document.getElementById(changeInfo.toUpdateID);
    if(newVal !== toUpdate[changeInfo.name]) {
      //ensure a function passed from the parent is bound to the parent
      if(typeof newVal == 'function') {
        newVal=newVal.bind(changeInfo.parent)
      }
      //add special "ref" property
      if(changeInfo.name=='.ref' && typeof newVal == 'function') newVal(toUpdate);
      //also gets rid of the preceding dot that indicates it's a property
      toUpdate[changeInfo.name.slice(1)]=newVal
    }

    //this is in case the ID has been updated during this call of applyChanges
    if(changeInfo.toUpdateID !== toUpdate.getAttribute('id'))
      changeInfo.toUpdateID=toUpdate.getAttribute('id');
  },
  replaceRange(changeInfo,newVal,toUpdate) {
    //changeInfo={type:'replaceRange',keyType,startNode,endNode,oldVal}
    //newVal=[...{keys,strings,key}]
    //oldVal=[...{keys,strings,key,rootEl,changeInfo}] or undefined
    switch(changeInfo.keyType) {
      case 'keyedTagArray': //result of keyedTag
        const newKeyMap=new Map();
        for(let i=0;i<newVal.length; i++) newKeyMap.set(newVal[i].key,newVal[i])

        //cases: moved existing node(s), deleted node(s), added node(s)
        let currentNode=document.getElementById(changeInfo.startNodeID)
        const endNode=document.getElementById(changeInfo.endNodeID)

        for(let i=0;i<newVal.length; i++) {
          if(!changeInfo.oldKeyMap || !(changeInfo.oldKeyMap.has(newVal[i].key))) {
            const newChanges=this.identifyKeyChanges(newVal[i].strings,newVal[i].keys)
            newVal[i].changeInfo=newChanges.changeInfo
            newVal[i].rootEl=newChanges.rootEl
            currentNode.insertAdjacentElement('afterend',newChanges.rootEl)
            applyChanges(newChanges.changeInfo,newVal[i].keys)
            currentNode=newChanges.rootEl
          } else { //same key or key exists but was re-ordered
            const old=changeInfo.oldKeyMap.get(newVal[i].key)
            newVal[i].changeInfo=old.changeInfo
            newVal[i].rootEl=old.rootEl
            if(currentNode.nextElementSibling !== old.rootEl) //if key was reordered
              currentNode.insertAdjacentElement('afterend',old.rootEl)
            applyChanges(old.changeInfo,newVal[i].keys)
            currentNode=old.rootEl
          }
        }
        //delete anything that's left over from the old that isn't in the new
        if(currentNode.nextSibling != endNode) {
          const range = document.createRange()
          range.setStartAfter(currentNode)
          range.setEndBefore(endNode)
          range.deleteContents()
        }
        changeInfo.oldVal=newVal
        changeInfo.oldKeyMap=newKeyMap
        break;
      case 'tagObject': //result of tag
        //force a reset if the string template has changed entirely (as in a conditional section of a template)
        //or if there's no oldVal, then it hasn't rendered yet so force a reset
        let rootEl;
        let innerChangeInfo;
        const forceReset=!changeInfo.oldVal 
        || changeInfo.oldVal.strings.length !== newVal.strings.length
        || changeInfo.oldVal.strings.some((string,index)=>string!== newVal.strings[index])

        if(forceReset) {
          const newTemplate= this.identifyKeyChanges(newVal.strings,newVal.keys,changeInfo.parent)
          rootEl=newTemplate.rootEl
          innerChangeInfo=newTemplate.changeInfo
        } else {
          rootEl=changeInfo.oldVal.rootEl
          innerChangeInfo=changeInfo.oldVal.innerChangeInfo
        }
        if(forceReset
          || changeInfo.oldVal.keys.length !== newVal.keys.length
          || changeInfo.oldVal.keys.some((key,index)=>key !== newVal.keys[index])) {
          const endNode=document.getElementById(changeInfo.endNodeID);
          if(changeInfo.oldVal==undefined) {
            endNode.parentElement.insertBefore(rootEl,endNode)
          }
          else if(endNode.previousElementSibling !== rootEl) {
            endNode.previousElementSibling.replaceWith(rootEl)
          }
          applyChanges(innerChangeInfo,newVal.keys)
          newVal.rootEl=rootEl
          newVal.innerChangeInfo=innerChangeInfo
          changeInfo.oldVal=newVal
        }
        break;
      case 'textarea':
        toUpdate.textContent=toUpdate.textContent.replace(changeInfo.toFind,newVal)
        break;
      case 'htmlElement':
        const end=document.getElementById(changeInfo.endNodeID); //todo: why does it error when I call it endNode?
        const startNode=document.getElementById(changeInfo.startNodeID);
        if(end.previousElementSibling==startNode)
          end.parentElement.insertBefore(newVal,end)
        else if(end.previousElementSibling !== newVal)
          end.previousElementSibling.replaceWith(newVal)
        break;
      default: //create text node
        if(changeInfo.oldVal !== newVal) {
          
          const endNode=document.getElementById(changeInfo.endNodeID);
          if(changeInfo.oldVal===undefined)
            endNode.parentElement.insertBefore(document.createTextNode(newVal),endNode)
          else
            endNode.previousSibling.replaceWith(document.createTextNode(newVal))
          changeInfo.oldVal=newVal
        }
        break;
    }
  }
} // diffObj


function applyChanges(changeInfo,newVals) {
  //get all the elements whose props/attributes will be updated since id could also be updated
  // const toUpdates=changeInfo.map(change=>change.changeType=='replaceRange' ? null : document.getElementById(change.toUpdateID))
  const toUpdates=[]
  for(let index in changeInfo) {
    let el=null;
    if(changeInfo[index].changeType=='replaceRange') {
      if(changeInfo[index].keyType == 'textarea') {
        el=document.getElementById(changeInfo[index].toUpdateID)
        el.textContent=changeInfo[index].templateText
      }
    } else {
      el=document.getElementById(changeInfo[index].toUpdateID)
    }
    toUpdates.push(el)
  }

  for(let index in changeInfo)
    diffObj[changeInfo[index].changeType](changeInfo[index],newVals[index],toUpdates[index]);
}

/**
 * like JSX, the templates should have a single root element
 * @param {element} parent 
 * @param {strings,keys} output of a tagged template literal created with the "html" function
 */
function parse(parent,{strings,keys}) {
  let changeInfo,rootEl,oldStrings;
  let isDirty=false;
  if(templateCache.has(parent)) {
    const cached=templateCache.get(parent)
    changeInfo=cached.changeInfo;
    rootEl=cached.rootEl;
    oldStrings=cached.strings;

    if(oldStrings.length !== strings.length
      || oldStrings.some((item,index)=>item !== strings[index])) isDirty=true;
  } 
  else isDirty=true;

  if(isDirty) {
    const result=diffObj.identifyKeyChanges(strings,keys,parent)
    changeInfo=result.changeInfo
    rootEl=result.rootEl
    templateCache.set(parent,result)
  }

  if(parent.firstElementChild !== rootEl) {
    const range = document.createRange()
    range.selectNodeContents(parent)
    range.deleteContents()
    parent.appendChild(rootEl)
  }

  applyChanges(changeInfo,keys)
  return rootEl
}
function keyed(key) { 
  return (strings,...keys)=>{
    return {strings:strings.raw,key,keys}
  }
}

function html(strings,...keys) {
  return {strings:strings.raw,keys}
}  


/**
 * defines a custom element with the same signature as customElements.define()
 * @param tagName is the element name
 * @param theClass is the element class
 * @param extendsParam is an object with an "extends" property indicating which element it extends
 */
const define = (tagName,theClass,extendsParam=undefined)=>{
  
  class inherited extends theClass {
    /**********Re-render at Will************/
    render() {
      if(super.render !== undefined && typeof super.render=='function') {
        const result=super.render.bind(this)({html,keyed,state:this.state,cached:this.cached})
        if(result==undefined) throw new Error('Render function returned undefined. Please make sure your render function is returning the template, e.g. return html`<my-template />`')
        parse(this,result)
      }
    }
    /*******************Event Handling********************/
    handleEvent(e) {
      if(e.currentTarget==window) this.listeners.window[e.type].bind(this,e)()
      else this.listeners[e.type].bind(this,e)()
    }

    /*****************Auto-Removing of Listeners***************/
    disconnectedCallback() {
      if(super.disconnectedCallback) super.disconnectedCallback();
      if(this.listeners)
        Object.keys(this.listeners)
            .filter(type=>type !== 'window')
            .forEach(type=>this.removeEventListener(type,this))
      if(this.listeners && this.listeners.window) 
        Object.keys(this.listeners.window).forEach(type=>window.removeEventListener(type,this))
    }

    /**********Data-Bound Property***********/
    set observe(newState) {
      Object.assign(this.state,newState)
    }
    

    connectedCallback() {
      /*********Memoized-once Functions***********/
      const cache={}
      function memoizeOnce(myFn,funcName,thisContext) {
        return (...args)=>{
          //if every arg is the same as the previous args, return the cached value
          if(cache[funcName] && cache[funcName].lastParams.length===args.length
             && cache[funcName].lastParams.every((arg,i)=>args[i]===arg)) {
            return cache[funcName].lastResult
          } else {
            const result=myFn.apply(thisContext,args)
            cache[funcName]={lastParams:args,lastResult:result}
            return result
          }
        }
      }
      const newCached={}
      for(const funcName in this.cached) {
         newCached[funcName]=memoizeOnce(this.cached[funcName],funcName,this)
      }
      this.cached=newCached

      /*****************Auto-Adding of Listeners***************/
      if(this.isConnected && this.listeners) {
        Object.keys(this.listeners)
            .filter(type=>type !== 'window')
            .forEach(type=>this.addEventListener(type,this))
        if(this.listeners.window) Object.keys(this.listeners.window).forEach(type=>window.addEventListener(type,this))
      }

      /*****************Slot Handling*************************/
      //putting it here because it has to happen before the first render() which changes the childNodes
      if(this.slots == undefined) {
        const newSlots = {default:[]};
        Array.from(this.childNodes).forEach(el => { //handle slots
          //if it's not a text node, see if it has a slot name
          const slotName=el.getAttribute && el.getAttribute('slot')
          //if not, just add it to slot defaults
          slotName? newSlots[slotName] = el : newSlots.default.push(el)
        });
        this.slots=newSlots
      }

      /*****************Data Binding***************/
      let debounce;
      if(this.state) {
        const setter=(target, prop, value)=>{
          if(this.state[prop]==value) return true;
          const isSuccessful=Reflect.set(target,prop,value)
          //debounce so it doesn't fire off multiple renders when you're setting multiple values at once
          if(debounce) window.cancelAnimationFrame(debounce); //if there's another outstanding request, cancel it
          debounce=window.requestAnimationFrame(this.render.bind(this)) //request a render on the next animation frame
          return isSuccessful
        }
        this.state=new Proxy(this.state,{ set:setter })
      } else this.state={}
      
      //note: once an element is connected, it can use getAttribute
      if(super.connectedCallback) super.connectedCallback();
      if(this.render) this.render()
    } //end connectedCallback
  } //end class definition
  customElements.define(tagName,inherited,extendsParam)
}

/*****************Shared State Management***************/
/**
 * Create a store for shared state.
 * You probably also want to break up your shared state into different stores when it makes sense to do so
 * Note: it converts Maps/Sets/Symbols to their JSON equivalents objects/{}/undefined, so better just use plain objects
 * @param options an object with state, actions, a css selector and an event name:
 * @param options.state is the initial state of the store
 * @param options.actions are the actions you can take which will mutate the state
 * @param options.actions[*] each action should be a function that takes the current state and any additional params and returns the part of the state that changed (can be an empty object if nothing changed). Can be async
 * @param options.event is the name of the custom event which will be dispatched to the interested elements
 * @return {getState,actions} getState will get the current state, actions will apply the actions and will return a promise when done applying
 */
function createStore({state,actions,event}) {
  const clone=(obj)=>JSON.parse(JSON.stringify(obj))
  //first deep clone the given state for safety against tampering
  let innerState=clone(state), debounce

  function commit(result) {
    innerState=Object.assign(innerState,clone(result))
    
    
    if(debounce) window.cancelAnimationFrame(debounce);
    debounce=window.requestAnimationFrame(()=>
    //dispatch a custom event to the window with the shared state
      window.dispatchEvent(new CustomEvent(event,{detail:clone(innerState)})))
    //a deep clone is made of the resulting state before sending it out, for safety against tampering
  }
  //return getState along with the given actions after they've been partially applied to the inner state
  return Object.keys(actions).reduce((accum,curr)=>({
    ...accum,
    [curr]:(...args)=>{
      //call the action with a clone of the state
      commit(actions[curr](...[clone(innerState),...args]))
    }
  }),{getState:()=>clone(innerState)})
}



module.exports={
  define,
  createStore
}