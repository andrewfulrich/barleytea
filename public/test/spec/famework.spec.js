describe('framework',function() {
  //todo: convert code examples to tests
  it('detect the correct "this" within a cached function called from connectedCallback',function() {
    const content=document.getElementById('content')
    let cachedThis;
    define('test-cached-this',class testCachedThis extends HTMLElement {
      connectedCallback() {
        this.cached.test()
      }
      cached={
        test() {
          cachedThis=this
        }
      }
    })
    const testEl=document.createElement('test-cached-this')
    content.appendChild(testEl)
    expect(cachedThis).toEqual(testEl)
  })
})