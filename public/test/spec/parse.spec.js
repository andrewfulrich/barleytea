
describe('parse',function() {
  it('updates attributes',function() {
    const content=document.getElementById('content')
    templateCache.delete(content);
    parse(content,html`<h1 id="testEl" data-test=${'hi'}>
    Hello World
    </h1>`)
    const testEl=document.getElementById('testEl')
    expect(testEl).toBeDefined()
    expect(testEl.getAttribute('data-test')).toEqual('hi')
    parse(content,html`<h1 id="testEl" data-test=${'hey'}>
    Hello World
    </h1>`)
    expect(testEl.getAttribute('data-test')).toEqual('hey')
  })

  it('updates properties',function() {
    const content=document.getElementById('content')
    parse(content,html`<h1 id="testEl" .test=${'hi'}>
    Hello World
    </h1>`)
    const testEl=document.getElementById('testEl')
    expect(testEl).toBeDefined()
    expect(testEl.test).toEqual('hi')
    parse(content,html`<h1 id="testEl" .test=${'hey'}>
    Hello World
    </h1>`)
    expect(testEl.test).toEqual('hey')
  })
  it('replaces ranges',function() {
    const content=document.getElementById('content')
    parse(content,html`<h1 id="testEl">${'Hello'} World</h1>`);
    const testEl=document.getElementById('testEl')
    expect(testEl).toBeDefined()
    expect(testEl.textContent).toEqual('Hello World')
    parse(content,html`<h1 id="testEl">${'Hey'} World</h1>`);
    expect(testEl.textContent).toEqual('Hey World')
  })
  it('maps values',function() {
    const content=document.getElementById('content')
    parse(content,html`<ul>${['bleep','blop','bloop'].map((item,index)=>
      keyed(index)`<li>${item}</li>`
    )}</ul>`);
    const testEl1=content.firstElementChild.firstElementChild.nextElementSibling
    expect(testEl1).toBeDefined()
    expect(testEl1.textContent).toEqual('bleep')
    const testEl2=testEl1.nextElementSibling
    expect(testEl2).toBeDefined()
    expect(testEl2.textContent).toEqual('blop')
    const testEl3=testEl2.nextElementSibling
    expect(testEl3).toBeDefined()
    expect(testEl3.textContent).toEqual('bloop')
  })
  it('updates mapped values',function() {
    const content=document.getElementById('content')
    parse(content,html`<ul>${['bleep','blop','bloop'].map((item,index)=>
      keyed(index)`<li>${item}</li>`
    )}</ul>`);
    const testEl2=content.firstElementChild.firstElementChild.nextElementSibling.nextElementSibling
    expect(testEl2).toBeDefined()
    expect(testEl2.textContent).toEqual('blop')
    const testEl3=testEl2.nextElementSibling
    expect(testEl2.nextElementSibling).toEqual(testEl3);
    parse(content,html`<ul>${['bleep','blopadiddly','bloop'].map((item,index)=>
      keyed(index)`<li>${item}</li>`
    )}</ul>`);
    expect(testEl2.textContent).toEqual('blopadiddly')
    expect(testEl2.nextElementSibling).toEqual(testEl3)
  })
  it('clears all mapped values',function() {
    const content=document.getElementById('content')
    parse(content,html`<ul>${['bleep','blop','bloop'].map((item,index)=>
      keyed(index)`<li>${item}</li>`
    )}</ul>`);
    const testEl2=content.firstElementChild.firstElementChild.nextElementSibling.nextElementSibling
    expect(testEl2).toBeDefined()
    expect(testEl2.textContent).toEqual('blop')
    const testEl3=testEl2.nextElementSibling
    expect(testEl2.nextElementSibling).toEqual(testEl3);
    parse(content,html`<ul>${[].map((item,index)=>
      keyed(index)`<li>${item}</li>`
    )}</ul>`);
    const listElements=content.querySelectorAll('li')
    expect(listElements.length).toEqual(0)
  })
  it('updates mapped values even with dynamic ids',function() {
    const content=document.getElementById('content')
    parse(content,html`<ul id="new">${['bleep','blop','bloop'].map((item,index)=>
      keyed(index)`<li id=${item}>${item}</li>`
    )}</ul>`);
    const testEl2=content.firstElementChild.firstElementChild.nextElementSibling.nextElementSibling
    expect(testEl2).toBeDefined()
    expect(testEl2.textContent).toEqual('blop')
    const testEl3=testEl2.nextElementSibling
    expect(testEl2.nextElementSibling).toEqual(testEl3);
    parse(content,html`<ul id="new">${['bleep','blopadiddly','bloop'].map((item,index)=>
      keyed(index)`<li id=${item}>${item}</li>`
    )}</ul>`);
    expect(testEl2.textContent).toEqual('blopadiddly')
    expect(testEl2.nextElementSibling).toEqual(testEl3)
  })
  it('handles if statements',function() {
    const content=document.getElementById('content')
    let flipper=true
    const render=()=>{
      parse(content,html`<div>${flipper ? 
        html`<h1>True</h1>`
        : html`<h2>False</h2>`
        }</div>`);
    }
    render()
    expect(content.firstElementChild.firstElementChild.nextElementSibling.nodeName).toEqual('H1')
    flipper=false
    render()
    expect(content.firstElementChild.firstElementChild.nextElementSibling.nodeName).toEqual('H2')
  })
  it('applies an element to a function passed to its special .ref property',function() {
    const content=document.getElementById('content')
    var triggered=false
    const trigger=el=>{
      triggered=true
      expect(el).toEqual(document.getElementById('testEl'))
    }
    const render=()=>parse(content,html`<div id="testEl" .ref=${trigger}>hi</div>`)
    render()
    expect(triggered).toEqual(true)
  })
  it('handles a single-character element',function() {
    const content=document.getElementById('content')
    var triggered=false
    const trigger=el=>{
      triggered=true
      expect(el).toEqual(document.getElementById('testEl'))
    }
    const render=()=>parse(content,html`<p id="testEl" >hi</p>`)
    render()
    expect(content.textContent).toEqual('hi')
  })
})