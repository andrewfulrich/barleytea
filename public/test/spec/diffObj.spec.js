function render(rootEl) {
  const parent=document.getElementById('content')
  const range = document.createRange()
  range.selectNodeContents(parent)
  range.deleteContents()
  parent.appendChild(rootEl)
}
describe('diffObj', function() {
  describe('convertSelfClosing',function() {
    it('closes tags that need to be closed', function() {
      const input=`<div><custom /><other></other><one-more/></div>`
      const output=`<div><custom></custom><other></other><one-more></one-more></div>`
      expect(diffObj.convertSelfClosing(input)).toEqual(output)
    })
    it('does not close, for example, input tags', function() {
      const input=`<div><input value="hi"><other></other><one-more/></div>`
      const output=`<div><input value="hi"><other></other><one-more></one-more></div>`
      expect(diffObj.convertSelfClosing(input)).toEqual(output)
    })
  })
  
  describe('identifyKeyChanges',function() {
    it('throws an error when the template has no root element',function() {
      const input=`Hello World`
      let message='Error did not throw!'
      try {
        diffObj.identifyKeyChanges([input],[])
      } catch(e) {
        message=e.message
      }
      expect(message).toContain('template must have a root element')
    })
    it('throws an error when the template has more than one root element',function() {
      const input=`<div></div><div></div>`
      let message='Error did not throw!'
      try {
        diffObj.identifyKeyChanges([input],[])
      } catch(e) {
        message=e.message
      }
      expect(message).toContain('template must have a single root element')
    })
    
    it('identifies an updateProp key and produces "template" html',function() {
      const input={
        strings:['<div .withoutQuotes=','><span .withQuotes="','"></span></div>'],
        keys: [0,()=>{}]
      }
      const output=diffObj.identifyKeyChanges(input.strings,input.keys)
      expect(output.rootEl).toBeDefined()
      expect(output.rootEl.nodeName).toEqual('DIV')
      expect(output.rootEl.firstChild.nodeName).toEqual('SPAN')

      expect(output.changeInfo).toBeDefined()
      expect(output.changeInfo[0]).toBeDefined()
      expect(output.changeInfo[0].changeType).toEqual('updateProp')
      expect(output.changeInfo[0].name).toEqual('.withoutQuotes')
      render(output.rootEl);
      const toUpdate=document.getElementById(output.changeInfo[0].toUpdateID)
      expect(toUpdate).toEqual(output.rootEl)

      expect(output.changeInfo).toBeDefined()
      expect(output.changeInfo[1]).toBeDefined()
      expect(output.changeInfo[1].changeType).toEqual('updateProp')
      expect(output.changeInfo[1].name).toEqual('.withQuotes')
      const toUpdate2=document.getElementById(output.changeInfo[1].toUpdateID)
      expect(toUpdate2).toEqual(output.rootEl.firstElementChild)
    })
    it('identifies an updateAttr key and produces "template" html',function() {
      const input={
        strings:['<div withoutQuotes=','><span withQuotes="','"></span></div>'],
        keys: [0,()=>{}]
      }
      const output=diffObj.identifyKeyChanges(input.strings,input.keys)
      expect(output.rootEl).toBeDefined()
      expect(output.rootEl.nodeName).toEqual('DIV')
      expect(output.rootEl.firstChild.nodeName).toEqual('SPAN')

      expect(output.changeInfo).toBeDefined()
      expect(output.changeInfo[0]).toBeDefined()
      expect(output.changeInfo[0].changeType).toEqual('updateAttr')
      expect(output.changeInfo[0].name).toEqual('withoutQuotes')
      render(output.rootEl);
      const toUpdate=document.getElementById(output.changeInfo[0].toUpdateID)
      expect(toUpdate).toEqual(output.rootEl)

      expect(output.changeInfo).toBeDefined()
      expect(output.changeInfo[1]).toBeDefined()
      expect(output.changeInfo[1].changeType).toEqual('updateAttr')
      expect(output.changeInfo[1].name).toEqual('withQuotes')
      const toUpdate2=document.getElementById(output.changeInfo[1].toUpdateID)
      expect(toUpdate2).toEqual(output.rootEl.firstElementChild)
    })
    it('identifies a default replaceRange key and produces "template" html',function(){
      const input={strings: ['<div>Hello ','</div>'],keys:['World']}
      const output = diffObj.identifyKeyChanges(input.strings,input.keys)
      expect(output.rootEl).toBeDefined()
      expect(output.rootEl.firstChild.textContent).toEqual('Hello ')
      expect(output.rootEl.firstChild.nextSibling.nodeName).toEqual('TEMPLATE')
      expect(output.rootEl.lastChild.nodeName).toEqual('TEMPLATE')

      expect(output.changeInfo).toBeDefined()
      expect(output.changeInfo[0]).toBeDefined()
      expect(output.changeInfo[0].changeType).toEqual('replaceRange')
      expect(output.changeInfo[0].keyType).toEqual('')
      render(output.rootEl)
      const startNode=document.getElementById(output.changeInfo[0].startNodeID)
      const endNode=document.getElementById(output.changeInfo[0].endNodeID) 
      expect(startNode.nodeName).toEqual('TEMPLATE')
      expect(endNode.nodeName).toEqual('TEMPLATE')
    })
    it('identifies a keyedTagArray replaceRange key and produces "template" html',function() {
      const input={
        strings:['<div> Hello ','</div>'],
        keys:[
          [
            {
              strings:['<span>','</span>'],
              keys:['World'],
              key:0
            }
          ]
        ]
      }
      const output= diffObj.identifyKeyChanges(input.strings,input.keys)
      expect(output.rootEl).toBeDefined()

      expect(output.changeInfo).toBeDefined()
      expect(output.changeInfo[0]).toBeDefined()
      expect(output.changeInfo[0].changeType).toEqual('replaceRange')
      expect(output.changeInfo[0].keyType).toEqual('keyedTagArray')
      render(output.rootEl)
      const startNode=document.getElementById(output.changeInfo[0].startNodeID)
      const endNode=document.getElementById(output.changeInfo[0].endNodeID) 
      expect(startNode.nodeName).toEqual('TEMPLATE')
      expect(endNode.nodeName).toEqual('TEMPLATE')
    })
    it('throws an error when a key is an array with an unacceptable structure',function() {
      const input={
        strings:['<div> Hello ','</div>'],
        keys:[
          [{
              strings:['<span>','</span>'],
              key:0
            }]
        ]
      }
      expect(()=>diffObj.identifyKeyChanges(input.strings,input.keys))
        .toThrow(new Error('Array input must be an array of objects of the form {strings,keys,key}. You can use the "keyed" function, e.g. "keyed(yourKey)`<your-template />`'))
    })

    it('identifies a tagObject replaceRange key and produces "template" html',function() {
      const input={
        strings:['<div> Hello ','</div>'],
        keys:[{
            strings:['<span>','</span>'],
            keys:['World']
          }]
      }
      const output= diffObj.identifyKeyChanges(input.strings,input.keys)
      expect(output.rootEl).toBeDefined()
      
      expect(output.changeInfo).toBeDefined()
      expect(output.changeInfo[0]).toBeDefined()
      expect(output.changeInfo[0].changeType).toEqual('replaceRange')
      expect(output.changeInfo[0].keyType).toEqual('tagObject')
      render(output.rootEl)
      const startNode=document.getElementById(output.changeInfo[0].startNodeID)
      const endNode=document.getElementById(output.changeInfo[0].endNodeID) 
      expect(startNode.nodeName).toEqual('TEMPLATE')
      expect(endNode.nodeName).toEqual('TEMPLATE')
    })
  })

  it('identifies a textarea replaceRange key and produces "template" html',function() {
    const input = {
      strings:['<textarea> hello ','</textarea>'],
      keys:['world']
    }
    const output= diffObj.identifyKeyChanges(input.strings,input.keys)
    expect(output.changeInfo).toBeDefined()
    expect(output.changeInfo[0]).toBeDefined()
    expect(output.changeInfo[0].changeType).toEqual('replaceRange')
    expect(output.changeInfo[0].keyType).toEqual('textarea')
    render(output.rootEl)
    expect(output.rootEl.hasAttribute('id')).toBeTrue()
    expect(output.rootEl.textContent).toContain('hello <')
  })
  
  describe('updateAttr',function() {
    it('sets the attribute of the given element if the attribute does not exists',function(){
      const toUpdate=document.createElement('div')
      toUpdate.setAttribute('id','toUpdate')
      const changeInfo={
        type:'attributeChange',
        name:'myAttr',
        toUpdateID:'toUpdate'
      }
      const newVal=0
      expect(toUpdate.hasAttribute('myAttr')).toBeFalse()
      diffObj.updateAttr(changeInfo,newVal,toUpdate)
      expect(toUpdate.getAttribute('myAttr')).toEqual('0')
    })
    it('updates the id of the given element when it notices it has changed',function(){
      const toUpdate=document.createElement('div')
      toUpdate.setAttribute('id','toUpdate-changed')
      const changeInfo={
        type:'attributeChange',
        name:'myAttr',
        toUpdateID:'toUpdate'
      }
      const newVal=0
      diffObj.updateAttr(changeInfo,newVal,toUpdate)
      expect(changeInfo.toUpdateID).toEqual('toUpdate-changed')
    })
    it('update the attribute of the given element if the attribute already exists',function(){
      const myEl=document.createElement('div')
      myEl.setAttribute('myAttr',1)
      myEl.setAttribute('id','toUpdate')
      const changeInfo={
        type:'attributeChange',
        name:'myAttr',
        toUpdateID:'toUpdate'
      }
      const newVal=0
      expect(myEl.getAttribute('myAttr')).toEqual('1')
      diffObj.updateAttr(changeInfo,newVal,myEl)
      expect(myEl.getAttribute('myAttr')).toEqual('0')
    })
  })

  describe('updateProp',function() {
    it('sets the property of the given element if the property does not exists',function(){
      const toUpdate=document.createElement('div')
      toUpdate.setAttribute('id','toUpdate')
      const changeInfo={
        type:'updateProp',
        name:'.myProp',
        toUpdateID:'toUpdate'
      }
      const newVal=0
      expect(toUpdate.myProp).toBeUndefined()
      diffObj.updateProp(changeInfo,newVal,toUpdate)
      expect(toUpdate.myProp).toEqual(0)
    })
    it('updates the id of the given element when it notices it has changed',function(){
      const toUpdate=document.createElement('div')
      toUpdate.setAttribute('id','toUpdate-changed')
      const changeInfo={
        type:'updateProp',
        name:'.myProp',
        toUpdateID:'toUpdate'
      }
      const newVal=0
      diffObj.updateProp(changeInfo,newVal,toUpdate)
      expect(changeInfo.toUpdateID).toEqual('toUpdate-changed')
    })
    it('update the property of the given element if the property already exists',function(){
      const myEl=document.createElement('div')
      myEl.setAttribute('id','toUpdate')
      myEl.myProp=1
      const changeInfo={
        type:'updateProp',
        name:'.myProp',
        toUpdateID:'toUpdate'
      }
      const newVal=0
      expect(myEl.myProp).toEqual(1)
      diffObj.updateProp(changeInfo,newVal,myEl)
      expect(myEl.myProp).toEqual(0)
    })

    it('functions passed as properties are bound to the parent that passed them',function(){
      const myEl=document.createElement('div')
      myEl.setAttribute('id','toUpdate')
      myEl.name='myEl'
      class parentClass {
        constructor() {
          this.name='parent'
        }
        getName() {
          return this.name;
        }
      }
      const parent = new parentClass()
      const changeInfo={
        parent,
        type:'updateProp',
        name:'.myProp',
        toUpdateID:'toUpdate'
      }
      const newVal=parent.getName
      diffObj.updateProp(changeInfo,newVal,myEl)
      expect(myEl.myProp()).toEqual('parent')
    })
  })

  describe('replaceRange',function() {
    it('inserts into the range the new value when no previous value exists',function() {
      const parent=document.createElement('div')
      const startNode=document.createElement('div')
      startNode.setAttribute('id','0')
      const endNode=document.createElement('div')
      endNode.setAttribute('id','1')
      parent.append(startNode)
      parent.append(endNode)
      render(parent)
      const changeInfo={
        type:'replaceRange',
        keyType:'',
        startNodeID:'0',
        endNodeID:'1'
      }
      diffObj.replaceRange(changeInfo,'World')
      expect(startNode.nextSibling.textContent).toEqual('World')
    })
    it('replaces the range contents with the new value when a previous value exists',function() {
      const parent=document.createElement('div')
      const startNode=document.createElement('div')
      startNode.setAttribute('id','0')
      const endNode=document.createElement('div')
      endNode.setAttribute('id','1')
      const inner=document.createTextNode('World')
      parent.append(startNode)
      parent.append(inner)
      parent.append(endNode)
      render(parent)
      const changeInfo={
        type:'replaceRange',
        keyType:'',
        startNodeID:'0',
        endNodeID:'1',
        oldVal:'World'
      }
      diffObj.replaceRange(changeInfo,'Waldo')
      expect(startNode.nextSibling.textContent).toEqual('Waldo')
      expect(startNode.nextSibling.nextSibling).toEqual(endNode)
    })
    it('replaces the range contents with the new value when the previous value was null',function() {
      const parent=document.createElement('div')
      const startNode=document.createElement('div')
      startNode.setAttribute('id','0')
      const endNode=document.createElement('div')
      endNode.setAttribute('id','1')
      const inner=document.createTextNode('World')
      parent.append(startNode)
      parent.append(inner)
      parent.append(endNode)
      render(parent)
      const changeInfo={
        type:'replaceRange',
        keyType:'',
        startNodeID:'0',
        endNodeID:'1',
        oldVal:null
      }
      diffObj.replaceRange(changeInfo,'Waldo')
      expect(startNode.nextSibling.textContent).toEqual('Waldo')
      expect(startNode.nextSibling.nextSibling).toEqual(endNode)
    })
    it('inserts a new fragment if given a tagObject and no previous value exists',function() {
      const parent=document.createElement('div')
      const startNode=document.createElement('div')
      startNode.setAttribute('id','0')
      const endNode=document.createElement('div')
      endNode.setAttribute('id','1')
      parent.append(startNode)
      parent.append(endNode)
      render(parent)
      const changeInfo={
        type:'replaceRange',
        keyType:'tagObject',
        startNodeID:'0',
        endNodeID:'1'
      }
      diffObj.replaceRange(changeInfo,{
        strings:['<h1>Hello','</h1>'],
        keys:[' World']
      })
      expect(startNode.nextSibling.textContent).toEqual('Hello World')
    })
    it('replaces a fragment if given a tagObject ',function() {
      const parent=document.createElement('div')
      const startNode=document.createElement('div')
      startNode.setAttribute('id','0')
      const endNode=document.createElement('div')
      endNode.setAttribute('id','1')
      const inner=document.createElement('span')
      inner.setAttribute('id','Waldo')
      parent.append(startNode)
      parent.append(inner)
      parent.append(endNode)
      render(parent)
      const changeInfo={
        type:'replaceRange',
        keyType:'tagObject',
        startNodeID:'0',
        endNodeID:'1',
        oldVal:{
          strings:['<span></span>'],
          keys:[]
        }
      }
      expect(parent.querySelector('#Waldo')).toEqual(inner)
      diffObj.replaceRange(changeInfo,{
        strings:['<h1>Hello','</h1>'],
        keys:[' World']
      })
      expect(startNode.nextSibling.textContent).toEqual('Hello World')
      expect(parent.querySelector('#Waldo')).toEqual(null)
    })
    it('renders a keyedTagArray',function() {
      const parent=document.createElement('div')
      const startNode=document.createElement('div')
      startNode.setAttribute('id','0')
      const endNode=document.createElement('div')
      endNode.setAttribute('id','1')
      parent.append(startNode)
      parent.append(endNode)
      render(parent)
      const changeInfo={
        type:'replaceRange',
        keyType:'keyedTagArray',
        startNodeID:'0',
        endNodeID:'1',
      }
      diffObj.replaceRange(changeInfo,[{
        strings:['<h1>Hello','</h1>'],
        keys:[' World'],
        key:0
      },
      {
        strings:['<h1>Here\'s','</h1>'],
        keys:[' Waldo'],
        key:1
      }])
      expect(startNode.nextSibling.textContent).toEqual('Hello World')
      expect(startNode.nextSibling.nextSibling.textContent).toEqual('Here\'s Waldo')
    })
    it('updates only a keyedTagArray\'s element when only it has changed',function() {
      const parent=document.createElement('div')
      const startNode=document.createElement('div')
      startNode.setAttribute('id','0')
      const endNode=document.createElement('div')
      endNode.setAttribute('id','1')
      parent.append(startNode)
      parent.append(endNode)
      render(parent)
      const changeInfo={
        type:'replaceRange',
        keyType:'keyedTagArray',
        startNodeID:'0',
        endNodeID:'1'
      }
      diffObj.replaceRange(changeInfo,[{
        strings:['<h1>Hello','</h1>'],
        keys:[' World'],
        key:0
      },
      {
        strings:['<h1>Here\'s','</h1>'],
        keys:[' Waldo'],
        key:1
      }])
      expect(startNode.nextSibling.textContent).toEqual('Hello World')
      expect(startNode.nextSibling.nextSibling.textContent).toEqual('Here\'s Waldo')
      const originalFirst=startNode.nextSibling
      diffObj.replaceRange(changeInfo,[{
        strings:['<h1>Hello','</h1>'],
        keys:[' World'],
        key:0
      },
      {
        strings:['<h1>Here\'s','</h1>'],
        keys:[' Wizrobe'],
        key:1
      }])
      expect(startNode.nextSibling).toEqual(originalFirst)
      expect(startNode.nextSibling.nextSibling.textContent).toEqual('Here\'s Wizrobe')
    })
    it('inserts new elements in a keyedTagArray while leaving existing ones',function() {
      const parent=document.createElement('div')
      const startNode=document.createElement('div')
      startNode.setAttribute('id','0')
      const endNode=document.createElement('div')
      endNode.setAttribute('id','1')
      parent.append(startNode)
      parent.append(endNode)
      render(parent)
      const changeInfo={
        type:'replaceRange',
        keyType:'keyedTagArray',
        startNodeID:'0',
        endNodeID:'1'
      }
      diffObj.replaceRange(changeInfo,[{
        strings:['<h1>Hello','</h1>'],
        keys:[' World'],
        key:0
      },
      {
        strings:['<h1>Here\'s','</h1>'],
        keys:[' Waldo'],
        key:1
      }])
      expect(startNode.nextSibling.textContent).toEqual('Hello World')
      expect(startNode.nextSibling.nextSibling.textContent).toEqual('Here\'s Waldo')
      const originalFirst=startNode.nextSibling
      const originalSecond=startNode.nextSibling.nextSibling
      diffObj.replaceRange(changeInfo,[{
          strings:['<h1>Hello','</h1>'],
          keys:[' World'],
          key:0
        },
        {
          strings:['<h1>Here\'s','</h1>'],
          keys:[' Wizrobe'],
          key:1
        },
        {
          strings:['<h1>Hello','</h1>'],
          keys:[' Wicket'],
          key:2
        }
      ])
      expect(startNode.nextSibling).toEqual(originalFirst)
      expect(startNode.nextSibling.nextSibling).toEqual(originalSecond)
      expect(startNode.nextSibling.nextSibling.nextSibling.textContent).toEqual('Hello Wicket')
    })
    it('inserts new elements in a keyedTagArray in the middle while leaving existing ones',function() {
      const parent=document.createElement('div')
      const startNode=document.createElement('div')
      startNode.setAttribute('id','0')
      const endNode=document.createElement('div')
      endNode.setAttribute('id','1')
      parent.append(startNode)
      parent.append(endNode)
      render(parent)
      const changeInfo={
        type:'replaceRange',
        keyType:'keyedTagArray',
        startNodeID:'0',
        endNodeID:'1'
      }
      diffObj.replaceRange(changeInfo,[{
        strings:['<h1>Hello','</h1>'],
        keys:[' World'],
        key:0
      },
      {
        strings:['<h1>Hello','</h1>'],
        keys:[' Wicket'],
        key:2
      }])
      expect(startNode.nextSibling.textContent).toEqual('Hello World')
      expect(startNode.nextSibling.nextSibling.textContent).toEqual('Hello Wicket')
      const originalFirst=startNode.nextSibling
      const originalSecond=startNode.nextSibling.nextSibling
      diffObj.replaceRange(changeInfo,[{
          strings:['<h1>Hello','</h1>'],
          keys:[' World'],
          key:0
        },
        {
          strings:['<h1>Here\'s','</h1>'],
          keys:[' Wizrobe'],
          key:1
        },
        {
          strings:['<h1>Hello','</h1>'],
          keys:[' Wicket'],
          key:2
        }
      ])
      expect(startNode.nextSibling).toEqual(originalFirst)
      expect(startNode.nextSibling.nextSibling.nextSibling).toEqual(originalSecond)
      expect(startNode.nextSibling.nextSibling.textContent).toEqual('Here\'s Wizrobe')
    })
    it('removes old elements when the new keyedTagArray does not have them and leaves existing ones',function() {
      const parent=document.createElement('div')
      const startNode=document.createElement('div')
      startNode.setAttribute('id','0')
      const endNode=document.createElement('div')
      endNode.setAttribute('id','1')
      parent.append(startNode)
      parent.append(endNode)
      render(parent)
      const changeInfo={
        type:'replaceRange',
        keyType:'keyedTagArray',
        startNodeID:'0',
        endNodeID:'1'
      }
      diffObj.replaceRange(changeInfo,[{
        strings:['<h1>Hello','</h1>'],
        keys:[' World'],
        key:0
      },
      {
        strings:['<h1>Here\'s','</h1>'],
        keys:[' Waldo'],
        key:1
      }])
      expect(startNode.nextSibling.textContent).toEqual('Hello World')
      expect(startNode.nextSibling.nextSibling.textContent).toEqual('Here\'s Waldo')
      const originalSecond=startNode.nextSibling.nextSibling
      diffObj.replaceRange(changeInfo,[{
        strings:['<h1>Here\'s','</h1>'],
        keys:[' Waldo'],
        key:1
      }])
      expect(startNode.nextSibling).toEqual(originalSecond)
      expect(startNode.nextSibling.nextSibling).toEqual(endNode)
    })
    it('renders dynamic textarea content',function() {
      const ta=document.createElement('textarea')
      ta.textContent='before replaceThis after'
      const changeInfo={
        type:'replaceRange',
        keyType:'textarea',
        toFind:'replaceThis'
      }
      diffObj.replaceRange(changeInfo,'middle stuff',ta)
      expect(ta.textContent).toEqual('before middle stuff after')
    })
  })
})