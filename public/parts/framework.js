/*
ISC License

Copyright (c) 2020 Andrew Ulrich

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/**********Templating************/
  
/**********************/

/**
 * defines a custom element with the same signature as customElements.define()
 * @param tagName is the element name
 * @param theClass is the element class
 * @param extendsParam is an object with an "extends" property indicating which element it extends
 */
const define = (tagName,theClass,extendsParam=undefined)=>{
  
  class inherited extends theClass {
    /**********Re-render at Will************/
    render() {
      if(super.render !== undefined && typeof super.render=='function') {
        const result=super.render.bind(this)({html,keyed,state:this.state,cached:this.cached})
        if(result==undefined) throw new Error('Render function returned undefined. Please make sure your render function is returning the template, e.g. return html`<my-template />`')
        parse(this,result)
      }
    }
    /*******************Event Handling********************/
    handleEvent(e) {
      if(e.currentTarget==window) this.listeners.window[e.type].bind(this,e)()
      else this.listeners[e.type].bind(this,e)()
    }

    /*****************Auto-Removing of Listeners***************/
    disconnectedCallback() {
      if(super.disconnectedCallback) super.disconnectedCallback();
      if(this.listeners)
        Object.keys(this.listeners)
            .filter(type=>type !== 'window')
            .forEach(type=>this.removeEventListener(type,this))
      if(this.listeners && this.listeners.window) 
        Object.keys(this.listeners.window).forEach(type=>window.removeEventListener(type,this))
    }

    /**********Data-Bound Property***********/
    set observe(newState) {
      Object.assign(this.state,newState)
    }
    /**********************/

    connectedCallback() {
      /*********Memoized-once Functions***********/
      const cache={}
      function memoizeOnce(myFn,funcName,thisContext) {
        return (...args)=>{
          //if every arg is the same as the previous args, return the cached value
          if(cache[funcName] && cache[funcName].lastParams.length===args.length
             && cache[funcName].lastParams.every((arg,i)=>args[i]===arg)) {
            return cache[funcName].lastResult
          } else {
            const result=myFn.apply(thisContext,args)
            cache[funcName]={lastParams:args,lastResult:result}
            return result
          }
        }
      }
      const newCached={}
      for(const funcName in this.cached) {
         newCached[funcName]=memoizeOnce(this.cached[funcName],funcName,this)
      }
      this.cached=newCached

      /*****************Auto-Adding of Listeners***************/
      if(this.isConnected && this.listeners) {
        Object.keys(this.listeners)
            .filter(type=>type !== 'window')
            .forEach(type=>this.addEventListener(type,this))
        if(this.listeners.window) Object.keys(this.listeners.window).forEach(type=>window.addEventListener(type,this))
      }

      /*****************Slot Handling*************************/
      //putting it here because it has to happen before the first render() which changes the childNodes
      if(this.slots == undefined) {
        const newSlots = {default:[]};
        Array.from(this.childNodes).forEach(el => { //handle slots
          //if it's not a text node, see if it has a slot name
          const slotName=el.getAttribute && el.getAttribute('slot')
          //if not, just add it to slot defaults
          slotName? newSlots[slotName] = el : newSlots.default.push(el)
        });
        this.slots=newSlots
      }

      /*****************Data Binding***************/
      let debounce;
      if(this.state) {
        const setter=(target, prop, value)=>{
          if(this.state[prop]==value) return true;
          const isSuccessful=Reflect.set(target,prop,value)
          //debounce so it doesn't fire off multiple renders when you're setting multiple values at once
          if(debounce) window.cancelAnimationFrame(debounce); //if there's another outstanding request, cancel it
          debounce=window.requestAnimationFrame(this.render.bind(this)) //request a render on the next animation frame
          return isSuccessful
        }
        this.state=new Proxy(this.state,{ set:setter })
      } else this.state={}
      /**********************/
      //note: once an element is connected, it can use getAttribute
      if(super.connectedCallback) super.connectedCallback();
      if(this.render) this.render()
    } //end connectedCallback
  } //end class definition
  customElements.define(tagName,inherited,extendsParam)
}

/*****************Shared State Management***************/
/**
 * Create a store for shared state.
 * You probably also want to break up your shared state into different stores when it makes sense to do so
 * Note: it converts Maps/Sets/Symbols to their JSON equivalents objects/{}/undefined, so better just use plain objects
 * @param options an object with state, actions, and an event name:
 * @param options.state is the initial state of the store
 * @param options.actions are the actions you can take which will mutate the state
 * @param options.actions[*] each action should be a function that takes the current state and any additional params and returns the part of the state that changed (can be an empty object if nothing changed). Can be async
 * @param options.event is the name of the custom event which will be dispatched to the interested elements
 * @return {getState,actions} getState will get the current state, actions will apply the actions and will return a promise when done applying
 */
function createStore({state,actions,event}) {
  const clone=(obj)=>JSON.parse(JSON.stringify(obj))
  //first deep clone the given state for safety against tampering
  let innerState=clone(state), debounce

  function commit(result) {
    innerState=Object.assign(innerState,clone(result))
    
    
    if(debounce) window.cancelAnimationFrame(debounce);
    debounce=window.requestAnimationFrame(()=>
    //dispatch a custom event to the window with the shared state
      window.dispatchEvent(new CustomEvent(event,{detail:clone(innerState)})))
    //a deep clone is made of the resulting state before sending it out, for safety against tampering
  }
  //return getState along with the given actions after they've been partially applied to the inner state
  return Object.keys(actions).reduce((accum,curr)=>({
    ...accum,
    [curr]:(...args)=>{
      //call the action with a clone of the state
      commit(actions[curr](...[clone(innerState),...args]))
    }
  }),{getState:()=>clone(innerState)})
}
/**********************/
