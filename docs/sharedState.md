# Shared State Management

Barleytea.js provides a way to share state across components without passing it directly. When sharing state across components, it's common practice to separate the state itself from the actions you can take on that state. In other words, it's common to have state as read-only except for a limited set of predefined actions used to mutate that state. This makes it easier to track what caused a state and also gives familiar approaches to things like time travel and rehydration.

This state management was inspired by the [Meiosis pattern](https://meiosis.js.org/), but it is simplified in that it uses events rather than streams.

## How to Use

Use the createStore function to create a shared state store. Pass in an object whose keys are:

 - `state`, which holds the initial state
 - `actions`, which holds all the actions you can take on the state. The return value of the action is Object.assigned onto the shared state, resulting in "shallow merging" of the state.
 - `event`, which is the custom event which will be fired to the `window` whenever state changes.

To "subscribe" to this store, just listen for its associated custom event on the `window`. When the event fires, its payload will be the current state of the store. The actions are in the return object of the `createStore` function along with a `getState` function to directly get the current state. These can be available globally, or if you use js modules, you can import/export them as you wish. How you wish to structure your app is up to you.

Here's an example that increments a shared counter. Each time it changes, it fires the `counterMutation` event to the window. The `count-clicker` is listening on the window for this event, and uses the payload to set its own internal state:

```javascript
const {increment,getState}=createStore({
  state: {
    counter:0,
    untouched:0
  },
  actions:{
    increment(state) {
      return {counter:state.counter+1}
    }
  },
  event:'counterMutation'
})

define('count-clicker',class countClicker extends HTMLElement {
  state={
    counter:0,
    untouched:0
  }
  listeners={
    window:{
      counterMutation(e) {
        Object.assign(this.state,e.detail)
      }
    }
  }
  render({html,state}) {
    return html`<div>
    <p>Count is ${state.counter}</p>
    <p>untouched is ${state.untouched}</p>
    <button .onclick="${increment}">Increment</button>
    </div>`
  }
})
```
```html
<count-clicker></count-clicker>
```
[See it in action here](https://jsfiddle.net/428cjomx/)

## Design Choices

- **No magic strings** In Redux/Vuex, you need to use a magic string to call your actions. NgRx imports the actions directly. In Barleytea.js, the actions are on the object returned from creating the store, and can be accessed directly.
- **Top-level state can be patched** NgRx/Redux state is immutable, so we need to return a full copy of the new state from a reducer. A common mistake that results is returning incomplete/stale irrelevant state from a reducer. A better approach is Vuex, whose mutators only touch the relevant state. Barleytea's approach is that your return value is Object.assigned onto the state, so you only have to return the relevant parts of the state.
- **Tamper Protection** With Redux, you have to promise yourself to never directly mutate what's passed into the reducer and only return new state (unless you pull in Immutable.js, yet another dependency). Vuex doesn't have this problem because you're touching the state directly inside your reducer, but since you're touching things directly you probably want to be extra careful. Barleytea's approach is to clone the state before providing it. This means referential comparisons won't work anymore, but that's a trade-off that seems worth it. Another trade-off is that the clone is as simple as possible, it's just `JSON.parse(JSON.stringify(state))`, which makes it easy to read and small in exchange for restricting to non-es6 data types. Feel free to modify the code to add your own cloning function if you want something more sophisticated.

## What about middleware?

Barleytea.js has no opinions around where to put your fetches and other async things that call your actions. If you feel you need it, maybe consider something like [flyd](https://github.com/paldepind/flyd) or something else, but also consider only adding this layer when it's truly called for, putting the functions where they make sense for now and [extracting](https://refactoring.com/catalog/extractFunction.html) them to somewhere more central later. ES6 has some basic promise composition functions already, and as we see Under the Hood below and in [Data Binding](./dataBinding.md), things like debouncing are pretty simple to implement.

## Dividing up Shared State

A common thing to do with shared state is to avoid gigantic, all-encompassing state objects.  Here's how this is approached in common frameworks:

| Barleytea.js | Vuex | Redux | Angular |
| --- | ---  | ---   | --- |
| multiple stores | namespaces | combineReducers | ActionReducerMap |

The common objection to using multiple stores as is done in Barleytea.js is that it makes it harder to do time travel and rehydration, so here's an example that does just that with multiple stores:

```javascript
let history=[]
const stores={}
function makeIntoHistoryRecorder(theStore,storeName) {
  stores[storeName]=theStore
  return new Proxy(theStore,{
    get: function (target, prop, receiver) {
      const theFunction=Reflect.get(target,prop,receiver);
      if (prop === "getState") {
        return theFunction
      }
      return new Proxy(theFunction,{
        apply: function(targetFunction, thisArg, argumentsList) {
        	const result=targetFunction(...argumentsList);
          history.push({
            ...Object.keys(stores).reduce((accum,curr)=>({...accum,[curr]:stores[curr].getState()}),{}),
            args:argumentsList,
            actionName:prop,
            storeName
          })
          return result
        }
      })
    }
  })
}

function rehydrateAll(index) {
  for(let storeName in stores) {
    stores[storeName].rehydrate(history[index][storeName])
  }
  history=index > 0 ? history.slice(0,index) : history.slice(0,1)
}

const countStore1=makeIntoHistoryRecorder(createStore({
  state:{
    count:0
  },
  actions:{
    setCount(state,count) {
      return {count:state.count+1}
    },
    rehydrate(state,newState) {
      return newState
    }
  },
  event:'count1Changed'
}),'countStore1')

const countStore2=makeIntoHistoryRecorder(createStore({
  state:{
    count:0
  },
  actions:{
    setCount(state,count) {
      return {count:state.count+1}
    },
    rehydrate(state,newState) {
      return newState
    }
  },
  event:'count2Changed'
}),'countStore2')


define('history-watcher',class customElement extends HTMLElement {
  state={
    historyIndex:0,
    count1:0,
    count2:0
  }
  listeners={
    window:{
      count1Changed(e) {
        this.state.count1=e.detail.count
        this.state.historyIndex=history.length-1
      },
      count2Changed(e) {
      	this.state.count2=e.detail.count
        this.state.historyIndex=history.length-1
      }
    }
  }
  
  render({html,state}) {
    function forward() {
      if(state.historyIndex < history.length-1) state.historyIndex++;
    }
    function backward() {
      if(state.historyIndex > 0) state.historyIndex--;
    }
    return html`<div>
    <h3>State at ${state.historyIndex}</h3>
    <pre>
${JSON.stringify(history[state.historyIndex],null,2)}
    </pre>
    <button .onclick=${e=>countStore1.setCount(state.count1++)}>Increment Counter 1</button>
    <button .onclick=${e=>countStore2.setCount(state.count2++)}>Increment Counter 2</button>
    <button .onclick=${forward}>Forward in time</button>
    <button .onclick=${backward}>Backward in time</button>
    <button .onclick=${()=>rehydrateAll(state.historyIndex)}>Rehydrate to this point</button>
    </div>`
  }
})
```
```html
<history-watcher></history-watcher>
```
[See it in action here](https://jsfiddle.net/3Lpd9khy/)

## Under the Hood

First, let's make the simplest thing I can think of that could possibly work which takes an object with initial state, actions, and an event name and fires the event with the updated state when an action is called:

```javascript
function createStore({state,actions,event}) {
  let innerState=state
  
  function commit(result) {
    innerState=result
    window.dispatchEvent(new CustomEvent(event,{detail:innerState}))
  }
  return Object.keys(actions).reduce((accum,curr)=>({
    ...accum,
    [curr]:(...args)=>{
      commit(actions[curr](...[innerState,...args]))
    }
  }),{getState:()=>innerState})
}
```

within the function, we are returning an object with the actions as methods. Before we return that object, we're committing the result to the inner state and dispatching an event to the window and the event is of the type specified by the event parameter. We also provide the handy `getState` method on the return object to get the current state at any time without having to wait for a change event to fire. This is a common feature across frameworks.

Instead of re-assigning the state directly, let's use `Object.assign` so that we don't have to always return the entire state from our actions. We could go even further and use a deep merge function like that provided in [mergerino](https://github.com/fuzetsu/mergerino) for example, but then we'd have to explicitly unassign anything that was part of the old state but isn't part of the new state, which can become a little onerous.

```javascript
function createStore({state,actions,event}) {
  let innerState=state

  function commit(result) {
    //use Object.assign for shallow patching
    innerState=Object.assign(innerState,result)
    window.dispatchEvent(new CustomEvent(event,{detail:innerState}))
  }
  return Object.keys(actions).reduce((accum,curr)=>({
    ...accum,
    [curr]:(...args)=>{
      commit(actions[curr](...[innerState,...args]))
    }
  }),{getState:()=>innerState})
}
```

Let's protect our state from tampering to ensure that the only way it's mutated is through actions. We do that by cloning the state whenever it's set or shared:

```javascript
function createStore({state,actions,event}) {
  const clone=(obj)=>JSON.parse(JSON.stringify(obj))
  let innerState=clone(state)

  function commit(result) {
    innerState=Object.assign(innerState,clone(result))
    window.dispatchEvent(new CustomEvent(event,{detail:clone(innerState)}))
  }
  return Object.keys(actions).reduce((accum,curr)=>({
    ...accum,
    [curr]:(...args)=>{
      commit(actions[curr](...[clone(innerState),...args]))
    }
  }),{getState:()=>clone(innerState)})
}
```

We saw in [Data Binding](./dataBinding.md) that debouncing is useful to prevent multiple firings when multiple things in the state are set at once. Let's add some debouncing before we broadcast the new state so we're not flooding our subscribers with calls:

```javascript
function createStore({state,actions,event}) {
  const clone=(obj)=>JSON.parse(JSON.stringify(obj))
  let innerState=clone(state), debounce

  function commit(result) {
    innerState=Object.assign(innerState,clone(result))
    if(debounce) window.cancelAnimationFrame(debounce);
    debounce=window.requestAnimationFrame(()=>
      window.dispatchEvent(new CustomEvent(event,{detail:clone(innerState)})))
  }
  return Object.keys(actions).reduce((accum,curr)=>({
    ...accum,
    [curr]:(...args)=>{
      commit(actions[curr](...[clone(innerState),...args]))
    }
  }),{getState:()=>clone(innerState)})
}
```

And that's minimalist-but-useful shared state management in about 16 lines of code.

Jump to:
 - [design goals](./designGoals.md)
 - [custom elements and lifecycle functions](./customElements.md)
 - [data binding](./dataBinding.md)
 - [slots](./slots.md)
 - [automatic registering and cleanup of listeners](./listenerHandling.md)
 - ["memoized-once" functions for computed values](./memoizedOnce.md)
 - [Templating](./templating.md)