# Why no routing?

I've personally found that putting separate pages into separate html files works really well for me. No having to hijack/re-invent the back button functionality, mess with mappings, or worry about code being included that's not relevant to the page, so you can avoid having to dream up things like tree-shaking and code splitting. It's an older way of doing things, for sure, but it cuts down on complexity and reduces the desire to use a bundler for development.

Here's what I do for each of my pages:

```html
<html>
  <head>
    <script src="barleytea.js"></script>
    ... (this page's particular dependencies go here)
  </head>
  <body>
    ...
  </body>
</html>
```

That's it, nothing new. For production, [I did write a small bundler](https://www.npmjs.com/package/mowdown) to take html like the above and concatenate and minify scripts into separate bundles for separate pages, so don't feel like you have to use it. You could also get fancy with modern imports and stuff and then go with something like rollup.js to bundle it all. That seems like a nice way to do things too. :)

All that being said, _nothing prevents_ you from using browser-side routering with barleytea.js. [RLite](https://github.com/chrisdavies/rlite) looks like a decent hash-based router, or you could [write one yourself, like this](https://jsfiddle.net/Lxydmr5a/1/) or like [this more full-featured and heavily commented example](https://vanillajstoolkit.com/helpers/router/).

Jump to:
 - [design goals](./designGoals.md)
 - [custom elements and lifecycle functions](./customElements.md)
 - [data binding](./dataBinding.md)
 - [slots](./slots.md)
 - [automatic registering and cleanup of listeners](./listenerHandling.md)
 - ["memoized-once" functions for computed values](./memoizedOnce.md)
 - [shared state management](./sharedState.md)
 - [Templating](./templating.md)