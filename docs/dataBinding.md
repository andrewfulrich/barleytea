# Data-Binding

When state changes, sometimes you want your dom to automatically change. Barleytea.js wraps your state in a [proxy](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy) so that any `set` operation done to it will check to see if that operation changed anything and if it did, it triggers a re-render.

## How it works
Here's a simple example of an element which keeps a count:

```javascript
class custom extends HTMLElement {
  state={
    count:0
  }
  render({html,state}) {
    return html`<button .onclick=${e=>state.count++}>Count: ${state.count}</button>`
  }
}
define('custom-el',custom);
```
```html
<custom-el></custom-el>
```
[See it in action here](https://jsfiddle.net/f16kx39e/)

Notice we pass in the state as an object which maps keys to values. 

We also have a magic `observe` property we can use to set state from a parent element and kick off a re-render that way:
```javascript
class otherCustom extends HTMLElement {
  state={
    count:0
  }
  
  render({html,state}) {
    return html`<p> count is now ${state.count}</p>`
  }
}
define('other-custom',otherCustom)

class custom extends HTMLElement {
  state={
    count:0,
  }
  render({html,state}) {
    function onclick() {
      state.count++
    }
    return html`<div><button .onclick=${onclick}>Click</button><other-custom .observe=${{count:state.count}} /></div>`
  }
}
define('custom-el',custom);
```
```html
<custom-el></custom-el>
```
[See it in action here](https://jsfiddle.net/edaj3zkf/)

## Under the hood

If you remember from [Custom Elements](./customElements.md), we're wrapping customElements.define and extending the passed-in class:

```javascript
function define(tagName,theClass,extendsParam=undefined) {
  //...
  class inherited extends theClass {
    render() {
      if(super.render !== undefined && typeof super.render=='function')
        //call the super.render and parse the result to a DOM
    }
    //...
    connectedCallback() {
      if(super.connectedCallback) super.connectedCallback();
      //...
      this.render()
    }
  }
  
  customElements.define(tagName,inherited,extendsParam)
}
```

Now let's wrap the class's state object (if it's defined) in a [proxy with a set trap](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy) that calls render when the new value is different from the old:

```javascript
function define(tagName,theClass,extendsParam=undefined) {
  //...
  class inherited extends theClass {
    render() {
      if(super.render !== undefined && typeof super.render=='function')
        //call the super.render and parse the result to a DOM
    }
    //...
    connectedCallback() {
      if(super.connectedCallback) super.connectedCallback();
      //...

      /*****************Data Binding***************/
      if(this.state) {
        const setter=(target, prop, value)=>{
          if(target[prop] === value) return true;
          this.render()
          return Reflect.set(target,prop,value)
        }
        this.state=new Proxy(this.state,{ set:setter })
      } else this.state={}

      this.render()
    }
  }
  
  customElements.define(tagName,inherited,extendsParam)
}
```

Now let's debounce the re-rendering so it happens only once per animation frame at most:

```javascript
function define(tagName,theClass,extendsParam=undefined) {
  //...
  class inherited extends theClass {
    render() {
      if(super.render !== undefined && typeof super.render=='function')
        //call the super.render and parse the result to a DOM
    }
    //...
    connectedCallback() {
      if(super.connectedCallback) super.connectedCallback();
      //...

      /*****************Data Binding***************/
      if(this.state) {
        const setter=(target, prop, value)=>{
          if(target[prop] === value) return true;
          const isSuccessful=Reflect.set(target,prop,value)

          if(this._framework.debounce) window.cancelAnimationFrame(this._framework.debounce);
          this._framework.debounce=window.requestAnimationFrame(this.render.bind(this))
          return isSuccessful
        }
        this.state=new Proxy(this.state,{ set:setter })
      } else this.state={}

      this.render()
    }
  }
  
  customElements.define(tagName,inherited,extendsParam)
}
```

This cancels any outstanding request for the re-render on the next animation frame and creates a new request to do so any time the set trap is fired more than once before the next animation frame.

Now let's add that magic `observe` property so we can observe state passed in from a parent:

```javascript
function define(tagName,theClass,extendsParam=undefined) {
  //...
  class inherited extends theClass {
    render() {
      if(super.render !== undefined && typeof super.render=='function')
        //call the super.render and parse the result to a DOM
    }
    //...

    /**********Data-Bound Property***********/
    set observe(newState) {
      Object.assign(this.state,newState)
    }

    connectedCallback() {
      if(super.connectedCallback) super.connectedCallback();
      //...

      /*****************Data Binding***************/
      if(this.state) {
        const setter=(target, prop, value)=>{
          if(target[prop] === value) return true;
          const isSuccessful=Reflect.set(target,prop,value)

          if(this._framework.debounce) window.cancelAnimationFrame(this._framework.debounce);
          this._framework.debounce=window.requestAnimationFrame(this.render.bind(this))
          return isSuccessful
        }
        this.state=new Proxy(this.state,{ set:setter })
      } else this.state={}

      this.render()
    }
  }
  
  customElements.define(tagName,inherited,extendsParam)
}
```

It's just a setter on our class which merges the `newState` into `this.state`. We got data binding and a data observer in about 13 lines of code.

Jump to:
 - [design goals](./designGoals.md)
 - [custom elements and lifecycle functions](./customElements.md)
 - [slots](./slots.md)
 - [automatic registering and cleanup of listeners](./listenerHandling.md)
 - ["memoized-once" functions for computed values](./memoizedOnce.md)
 - [shared state management](./sharedState.md)
 - [Templating](./templating.md)