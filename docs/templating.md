# Templating

Barleytea.js's templating engine uses tagged template literals. The control logic is handled with JS, and there are only a few differences from standard HTML. It's by far the largest and most complex part of Barleytea.js, and you may find that something like [uhtml](https://github.com/WebReflection/uhtml) is a production-ready drop-in replacement, and also frees you from the restriction of having to have a single root element in your templates, along with some other nice things. The advantage of using Barleytea's is that it has no dependencies and how it works is documented below, which hopefully makes its internals easier to understand. Both options are made available on the [download page.](https://andrewfulrich.gitlab.io/barleytea/distroMaker.html)

[[_TOC_]]

## How to use it

### Basic Example

Here's a quick example:
```javascript
const username = "World";
class custom extends HTMLElement {
  render({html}) {
    return html`
    <h1>Hello ${userName}!</h1>
    `
  }
}
define('custom-el',custom);
```

```html
<custom-el></custom-el>
```

Note that the template is a return value for the render function of the custom element. It uses the `html` tag function that's passed into the render function. Templates always need a root element (just like JSX, but unlike uhtml).

Barleytea.js also exposes the templating engine's `parse` function directly, so you could use the templating by itself. `parse` takes an element as its first argument, which is the element you want to render the template into, and the tagged template literal as the second, for example 
```javascript
parse(myElment,html`<my-template />`)
```
[see it in action](https://jsfiddle.net/Lxydmr5a/1/)

### Lists

Use the keyed tag function for a list:

```javascript
const usernames = ['Peter','Paul','Mary'];
class custom extends HTMLElement {
  render({html,keyed}) {
    return html`
    <div>Hello <ul>
    ${usernames.map((name,index)=>keyed(index)`<li>${name}</li>`)}
    </ul></div>
    `
  }
}
define('custom-el',custom);
```

```html
<custom-el></custom-el>
```

Note that the `keyed` function takes a unique key associated with that entry as its argument. Note that it's then returning another function which is the one that actually takes in the template.

### Attributes

Setting attributes is what you'd expect:
```javascript
const username = "World";
class custom extends HTMLElement {
  render({html}) {
    const myClass='my-class'
    return html`
    <h1 class=${myClass}>Hello ${userName}!</h1>
    `
  }
}
define('custom-el',custom);
```

```html
<custom-el></custom-el>
```

Please note that partial string attributes are not supported. so for example:
```javascript
html`
<div attribute="prefix-${suffix}` />
```
will throw an error. It should instead be:
```javascript
html`
<div attribute="${'prefix-'+suffix}` />
```
If you want partial string attributes, you might consider using [µhtml](https://github.com/WebReflection/uhtml), which is an alternative templating engine that's also offered from [the download page](https://andrewfulrich.gitlab.io/barleytea/distroMaker.html).

### Props

If you want to set a property on the element, prefix it with a dot:
```javascript
const username = "World";
class custom extends HTMLElement {
  render({html}) {
    const myClass='my-class'
    return html`
    <h1 .className=${myClass}>Hello ${userName}!</h1>
    `
  }
}
define('custom-el',custom);
```

```html
<custom-el></custom-el>
```

#### Special "ref" Prop
If you pass a function to the ref property, the function will be called upon initial render and whenever the function changes. The parameter of the function will be the element itself.

```javascript
class TodoList extends HTMLElement {
  state = {
    todos: ['write tests', 'debug code']
  }

  // this makes it possible to quickly add a todo on an "Enter" keypress
  handleKeyDown = e => {
    if (e.key === 'Enter' && e.target.value) {
      e.preventDefault()
      this.addTodo(e.target.value)
      this.inputElement.value=''
    }
  }

  // add a new todo with the given string value
  addTodo = todo => {
    this.state.todos = [...this.state.todos, todo]
    this.inputElement.value=''
  }

  // remove a todo by index
  removeTodo = idx => {
    this.state.todos = [...this.state.todos.slice(0, idx), ...this.state.todos.slice(idx + 1)]
  }

  render({ html, keyed, state }) {
    return html`
      <div>
        <h1>To dos:</h1>
        <input 
          placeholder='Enter a new item here' 
          .ref=${el => this.inputElement = el}
          .onkeydown=${this.handleKeyDown}>
        <button 
          .onclick=${this.addTodo}>Add</button>
        <ul>
          ${state.todos.map((todo, idx) => keyed(idx)`
            <li>
              ${todo} 
              <button
                .onclick=${() => this.removeTodo(idx)}>X</button>
            </li>`
          )}
        </ul>
      </div>
    `
  }
}

define('todo-list', TodoList)
```

```html
<todo-list></todo-list>
```

### Events

Use the vanilla js event handling properties to set a handler:
```javascript
const username = "World";
class custom extends HTMLElement {
  render({html}) {
    return html`
    <h1 .onclick=${e=>alert(`I've been clicked!`)}>Hello ${userName}!</h1>
    `
  }
}
define('custom-el',custom);
```

```html
<custom-el></custom-el>
```

## XSS Protection

Here's a really simple XSS attack:
```javascript
const userName=`
<button onclick="alert('xss attack from innerHTML')">
Click Me
</button>`

document.getElementById('component')
  .innerHTML=`
<h1>Hello ${userName}!</h1>
`;
```
```html
<div id="component">
</div>
```
[See it in action here](https://jsfiddle.net/5de90bvr/1/)

The above will display a button that can execute javascript instead of displaying what was expected to be a username. 

Trying the same thing in Barleytea.js looks like this:

```javascript
const userName=`
<button onclick="alert('xss attack from innerHTML')">
Click Me
</button>`

class custom extends HTMLElement {
  render({html}) {
    return html`
    <h1>Hello ${userName}!</h1>
    `
  }
}
define('custom-el',custom);
```

```html
<custom-el></custom-el>
```
[See it in action here](https://jsfiddle.net/f8eu9t4z/)

The output is that the username is treated like a string rather than code to be executed.

## Under the Hood

Here's how we added templating and rendering into our framework. If you remember from [Custom Elements](./customElements.md), we're wrapping customElements.define and extending the passed-in class:

```javascript
const define = (tagName,theClass,extendsParam=undefined)=>{
  //... (definition of parse, html, and keyed go here)

  class inherited extends theClass {
    render() {
      if(super.render !== undefined && typeof super.render=='function')
        parse(this,super.render.bind(this)({html,keyed,state:this.state,cached:this.cached}))
    }
    //...
    connectedCallback() {
      if(super.connectedCallback) super.connectedCallback();
      //...
      this.render()
    }
  }
  
  customElements.define(tagName,inherited,extendsParam)
}
```

The templating engine uses the `parse` function to parse the templates and handle the DOM diffing and such (see the two sections below for details on how all that works). What goes into it is the super class's render function if it's defined.

## DOM Diffing

DOM diffing ensures that only the things in the template that changed get re-rendered in the browser. This helps with performance and also helps prevent annoying glitches like losing focus on an input in your template when the template's underlying data updates.

The nice thing about string templates is that the sting parts almost never change, so we only need to keep track of updating the parts of the dom associated with the template's "keys" (sometimes also referred to as the "holes" in the template). These are the `${}` parts of the string template; the things that change dynamically.

The first time the template is run, it's parsed and the parts in the dom associated to each key are noted. Then, any other time it's run, it uses that info to update that part directly if the new value is different from the old value.

## How The Templating Part Works

```mermaid
graph TD
  

  subgraph "Apply Changes"
  S3[Get all target elements by ID] --> S4[Apply changes based on change type]
  S4 -->|Attribute| S10[Replace the attribute value]
  S4 -->|Prop| S11[Replace the prop value]
  S4 -->|Content| S12{{What type of content is it?}}
  S12 -->|Template content| S15[Identify/Retrieve template content info] --> S16[apply changes to that]
  S12 -->|Textarea Content| S14[Set it in the right place in the textContent of the textarea]
  S12 -->|Html Element Content| S23[Set it between the saved surrounding elements]
  S12 -->|Array content| S17[for each item]
  S17 --> S18{{Is this a new/old item?}}
  S18 -->|New| S19[Parse its template and add it to the list]
  S18 -->|Existing| S20[Identify/Retrieve template content info] --> S21[apply changes to that]
  S18 -->|No longer Existing| S22[Remove its root element]
  
  S12 -->|Anything Else| S13[Set it as text content]
  end

  

  subgraph "Parse"
  S1[Pass in an element and its template info] --> S2{{Element has existing template info?}}
  S2 -->|Yes| S3
  S9[Save template info] -->S3
  end
  
  subgraph "Identify Key Changes"
  S2 -->|No| S5
  S5{{Identify keys based on surrounding string parts}}
  S5 -->|Attribute or Prop Key| S6
  S5 -->|Content Key| S7
  S6[Save the ID of the element whose Prop/Attr changes] --> S8
  S7[Save the IDs of the surrounding elements] --> S8
  S8{{Done?}} -->|No| S5
  S8 -->|Yes| S9
  end
```

### 1. Pass in an element and its template

The parse method uses a cache associating elements with their template's string parts and key parts. To take advantage of the cache, tell it which element is associated with the template you want to parse.

Note that this means the `html` and `keyed` tag functions are actually very simple functions which are just directly returning the string and key parts of the template literal without doing any processing on them. All the processing actually happens within this parse function.

### 2. Element has existing template info?

Check whether the element already has cached template info. Also check whether the template strings themselves have changed (this can happen with conditional statements in your template). If the template has changed or if there is no cached info, create that info with `identifyKeyChanges` and then save it off.

### 3. Identify Key Changes

Basically there are 3 types of keys in a template: keys whose value is an attribute value, keys whose value is a property value, and keys whose value is a type of content.

1. For keys that are attributes or properties, save off the ID of the associated element and the property on the element that the key's value will be set to. If an ID doesn't exist for this element, make one.
2. For keys that are content, i.e. html ranges, create beginning and end nodes of that range, so we know to replace only what's in between those nodes. These "surrounding nodes" will usually be template nodes. For textareas, since templates can't exist in textareas, just save off the id of the textarea and its textContent.
3. Then take all that key info that tells us what exactly needs to change in this template and save it off to the cache

### 4. Apply Changes

Now that we have all the info to apply the changes directly, we do so. For attributes, props, nested templates, and text content, this is pretty straightforward. For keyed lists (the only allowed kind of list), it's a little more complicated since we have to compare the old list with the new list to re-render only what changed. We use the keys to compare the old and new list element templates and add new elements and remove old ones (see diagram).

The templating code weighs in at around 265 significant lines of code (sans comments/whitespace), so it's definitely the biggest and most complex part of the framework, but hopefully easy enough to understand.

Jump to:
 - [design goals](./designGoals.md)
 - [custom elements and lifecycle functions](./customElements.md)
 - [data binding](./dataBinding.md)
 - [slots](./slots.md)
 - [automatic registering and cleanup of listeners](./listenerHandling.md)
 - ["memoized-once" functions for computed values](./memoizedOnce.md)
 - [shared state management](./sharedState.md)