# Memoized-Once Functions

Barleytea.js provides a way to store the previous return values from a function so that if the same params are passed in a second time, the function doesn't have to re-run. You can use this to cache computations, or as a change callback.

Here's a comparison of how cached computations are used across some common frameworks. Notice cached computations in js frameworks existed at least as far back as 2010 in Knockout:

| Barleytea.js | React Hooks | Vue        | Angular           | Svelte   | Knockout       | Ember |
| ---           | ---         | ---        | ---               | ---      | ---            | --- |
| `cached` | `useMemo`        | `computed` | `mobX.@computed`† | `$:`     | `pureComputed` | `Ember.computed` |

† *third party library*

And here's a comparison of how change callbacks are used:

| Barleytea.js | React Hooks | Vue | Angular | Svelte |
| ---           | ---         | --- | ---     | --- |
| `cached` | `useEffect` | `watch` | `ngOnChanges`*/setters†/`Observable.distinctUntilChanged`†† | `$: callback(...thingsToWatch)` |

`*` *fires for changes to any property*

† *fires on any set operation even if unchanged*

†† **must integrate with middleware*

## How to Use

Add all the functions to the `cached` object in your class like so:

```javascript
define('custom-el',class custom extends HTMLElement {
  cached={
    //pseudocode
    myMemoizedFunc(params) {...}
  }
  render({cached}) {
    cached.myMemoizedFunc(params)
  }
})
```

The `define` will replace all those functions with memoized versions, so in your other functions you can assume `this.cache` has memoized-once versions of the functions. In the render function, for convenience, you can use the `cached` param instead of having to write `this.`.

Here's an example of using it as a cached computation:

```javascript
class custom extends HTMLElement {
  state={
    counts:[0,1,2,3,4,5,6,7],
    count:0
  }
  cached={
    expensiveTransform(counts) {
      console.log('I am running')
      return counts.filter(count=>count != 3)
            .map(count=>({
              count,
              message:'stuff'+count
            }))
    }
  }
  render({html,keyed,state,cached}) {
    return html`<div>
<button .onclick=${e=>state.counts=[...state.counts,5]}>click to add another</button>
<table>
  <thead><tr><td>Count</td><td>Message</td></thead>
  <tbody>
    ${cached.expensiveTransform(state.counts).map(({count,message},index)=>keyed(index)`
    <tr>
      <td>${count}</td><td>${message}</td>
    </tr>
    `)}
  </tbody>
</table>
<button .onclick=${e=>state.count++}>click to increment ${state.count}</button>
</div>`
  }
}
define('custom-el',custom);
```
```html
<custom-el></custom-el>
```
[See it in action here](https://jsfiddle.net/gzmhbowv/)

Our custom element has an array of counts. We want to run `expensiveTransform` (let's pretend it's really expensive to run) on that array only when necessary. It's being called in our render function. The button at the top adds to the array of counts, kicking off a render and forcing `expensiveTransform` to run. The button at the bottom, however, also kicks off a render but because it doesn't change the input for `expensiveTransform` we just get the cached result, which you can witness by seeing that a new console log statement does not appear.

Here's an example of using it as a change callback:

```javascript
class productList extends HTMLElement {
  state={
    categoryId:0,
    title:'',
    products:[],
  }
  cached={
    fakeFetch(tabId) {
      const products=[
        { title:'Industrial Flanges',products:['Steel Flange 10"','Copper Flange 5"','Aluminum Flange 3"']},
        { title:'Drop-forged Widgets',products:['Iron Widget 10"','Steel Widget 8"','Lead Widget 6"']},
        { title:'Spacely Sprockets',products:['Titanium Sprocket 12"','Aluminum Sprocket 2"','Steel Sprocket 1"']}
      ]
      this.state.title="loading"
      this.state.products=[]
      window.setTimeout(()=>{
        this.state.products=products[tabId].products
        this.state.title=products[tabId].title
      },1000)
    }
  }
  render({html,state,cached}) {
    cached.fakeFetch(state.categoryId)
    return html`
    <section>
      <h2>${state.title}</h2>
      <ul>
      ${state.products.map(product=>html`
        <li>${product}</li>
      `)}
      </ul>
    </section>
`
  }
}
define('product-list',productList)

class custom extends HTMLElement {
  state={
    categories:[
      {id:0,label:'Flanges'},
      {id:1,label:'Widgets'},
      {id:2,label:'Sprockets'}
    ],
    categoryId:0
  }
  render({html,keyed,state}) {
    return html`<div>
    ${state.categories.map((category,index)=>keyed(index)`
      <button .onclick=${e=>state.categoryId=category.id}>${category.label}</button>
    `)}
    <product-list .observe=${{categoryId:state.categoryId}}/>
    </div>
`
  }
}
define('custom-el',custom);
```
```html
<custom-el></custom-el>
```
[See it in action here](https://jsfiddle.net/q7djLgsc/)

Our custom element has a `product-list` element which we pass a category ID down to. We have buttons for switching categories.

Inside `product-list`, `fakeFetch` is a memoized function which sets the list of products based on the tab ID. It waits for a second before setting the results and in the meantime sets a loading state. It's only kicked off if the tab ID has changed, so in that sense it's like a watcher.

## Under the Hood

If you remember from [Custom Elements](./customElements.md), we're wrapping customElements.define and extending the passed-in class:

```javascript
function define(tagName,theClass,extendsParam=undefined) {
  //...
  class inherited extends theClass {
    render() {
      if(super.render !== undefined && typeof super.render=='function')
        //call the super.render and parse the result to a DOM
    }
    //...
    connectedCallback() {
      if(super.connectedCallback) super.connectedCallback();
      //...
      this.render()
    }
  }
  
  customElements.define(tagName,inherited,extendsParam)
}
```

Let's add in the memoizing code:

```javascript
function define(tagName,theClass,extendsParam=undefined) {
  //...
  class inherited extends theClass {
    render() {
      if(super.render !== undefined && typeof super.render=='function')
        //call the super.render and parse the result to a DOM
    }
    //...
    connectedCallback() {
      if(super.connectedCallback) super.connectedCallback();
      //...

      /*********Memoized-once Functions (i.e. functions only re-run when args change)***********/
      //cache stores the last params and last result of each function
      const cache={}
      //memoizeOnce returns a memoized-once version of any given function
      function memoizeOnce(myFn,funcName,thisContext) {
        return (...args)=>{
          if(cache[funcName] && cache[funcName].lastParams.length===args.length
             && cache[funcName].lastParams.every((arg,i)=>args[i]===arg)) {
            return cache[funcName].lastResult
          } else {
            const result=myFn.apply(thisContext,args)
            cache[funcName]={lastParams:args,lastResult:result}
            return result
          }
        }
      }
      const newCached={}
      for(const funcName in this.cached) {
         newCached[funcName]=memoizeOnce(this.cached[funcName],funcName,this)
      }
      this.cached=newCached

      this.render()
    }
  }
  
  customElements.define(tagName,inherited,extendsParam)
}
```

We're defining an internal function called `memoizeOnce` and an internal object called `cache`. The `cache` is going to store the last params and last result of each function. 

The `memoizeOnce` function returns a memoized once version of any function passed in. The way it works is it wraps the given function in a new function that first checks the cache to see if the info stored in the cache can be used instead of running the function. It also makes sure to bind the `this` context to the element so you get a meaningful `this` inside your memoized functions.

Then it loops through all the functions in `this.cache` and replaces them with their memoized-once versions. We got memoized-once functions in about 18 lines of code.

Jump to:
 - [design goals](./designGoals.md)
 - [custom elements and lifecycle functions](./customElements.md)
 - [data binding](./dataBinding.md)
 - [slots](./slots.md)
 - [automatic registering and cleanup of listeners](./listenerHandling.md)
 - [shared state management](./sharedState.md)
 - [Templating](./templating.md)