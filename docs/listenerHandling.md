# Listener Auto-Handling

Barleytea.js provides a way to automatically attach listeners when an element is added to the DOM and automatically remove them when the element is removed from the DOM. This is not something you see in most frameworks, but it can be quite convenient to have. Why? Well, two of the most common uses of lifecycle functions are:

1. Setting initial state
2. Setting up/Tearing down subscriptions

And the most common type of subscription to set up is event listeners.

And as far as setting up event listeners, [there are many ways to do it wrong:](https://web.archive.org/web/20161015014748/https://www.webreflection.co.uk/blog/2015/10/22/how-to-add-dom-events-listeners)

The common solution is to use the handleEvent method, as described in the article above, and that's what Barleytea.js does (see the Under the Hood section below for details).

## How to use it

You can simply define all your listeners in a `listeners` object in your class definition, like so:

```javascript
class customEl extends HTMLElement {
  listeners={
    click() {
      alert('clicked!')
    },
    mouseleave() {
      alert('mouse left!')
    }
  }
}
define('custom-el',customEl)
```
```html
<custom-el>click me</custom-el>
```
[See it in action here](https://jsfiddle.net/g3rLntox/)

You can also define listeners to attach to the window by putting them in a `window` object within `listeners`. Here's a scroll-to example that uses this (note this example also uses [data-binding with the state object](./dataBinding.md)):

```javascript
define('select-me',class focusOnMe extends HTMLElement {
  set selected(isSelected) {
    if(isSelected) {
      this.focus()
      this.scrollIntoView({ behavior: 'smooth' })
    }
  }
  set message(message) {
  	this.innerMessage=message
    this.render()
  }
  render({html}) {
    return html`<div>
      <p>${this.innerMessage}</p>
      <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    </div>`
  }
})

define('custom-el',class customElement extends HTMLElement {
  messages=['Press a key','Hi there!','Hello']
  state={
    messageIndex:0
  }
  listeners={
    window:{
      keydown(e) {
        this.state.messageIndex=(this.state.messageIndex+1)%3
      }
    }
  }
  render({html,state,keyed}) {
    return html`<div>
    ${this.messages.map((message,index)=>keyed(index)`
    <select-me 
      .message=${message}
      .selected=${index==state.messageIndex} />`)}
    </div>`
  }
})
```
```html
<custom-el></custom-el>
```
[See it in action here](https://jsfiddle.net/f0gwzLnp/)

## Under the Hood

If you remember from [Custom Elements](./customElements.md), we're wrapping customElements.define and extending the passed-in class:

```javascript
function define(tagName,theClass,extendsParam=undefined) {
  //...
  class inherited extends theClass {
    render() {
      if(super.render !== undefined && typeof super.render=='function')
        //call the super.render and parse the result to a DOM
    }
    //...
    connectedCallback() {
      if(super.connectedCallback) super.connectedCallback();
      //...
      this.render()
    }
  }
  
  customElements.define(tagName,inherited,extendsParam)
}
```

Let's add in listener adding and removal:

```javascript
function define(tagName,theClass,extendsParam=undefined) {
  //...
  class inherited extends theClass {
    render() {
      if(super.render !== undefined && typeof super.render=='function')
        //call the super.render and parse the result to a DOM
    }
    //...

    /*******************Event Handling********************/
    handleEvent(e) {
      if(e.currentTarget==window) this.listeners.window[e.type].bind(this,e)()
      else this.listeners[e.type].bind(this,e)()
    }

    /*****************Auto-Removing of listeners***************/
    disconnectedCallback() {
      if(super.disconnectedCallback) super.disconnectedCallback();
      if(this.listeners)
        Object.keys(this.listeners)
            .filter(type=>type !== 'window')
            .forEach(type=>this.removeEventListener(type,this))
      if(this.listeners && this.listeners.window) 
        Object.keys(this.listeners.window).forEach(type=>window.removeEventListener(type,this))
    }

    connectedCallback() {
      if(super.connectedCallback) super.connectedCallback();
      //...

      /*****************Auto-Adding of Listeners***************/
      if(this.isConnected && this.listeners) {
        Object.keys(this.listeners)
            .filter(type=>type !== 'window')
            .forEach(type=>this.addEventListener(type,this))
        if(this.listeners.window) 
          Object.keys(this.listeners.window).forEach(type=>window.addEventListener(type,this))
      }

      this.render()
    }
  }
  
  customElements.define(tagName,inherited,extendsParam)
}
```

We add in the [handleEvent](https://developer.mozilla.org/en-US/docs/Web/API/EventListener/handleEvent) method which delegates the handling to whatever listener functions we've defined. Then we attach those listeners in our `connectedCallback` lifecycle function and remove them in our `disconnectedCallback` function. That's listener auto-handling in about 20 lines of code.

Jump to:
 - [design goals](./designGoals.md)
 - [custom elements and lifecycle functions](./customElements.md)
 - [data binding](./dataBinding.md)
 - [slots](./slots.md)
 - ["memoized-once" functions for computed values](./memoizedOnce.md)
 - [shared state management](./sharedState.md)
 - [Templating](./templating.md)