# Design Goals of our Framework

- as close to **standard JS/HTML** as possible
- **ergonomically similar** to popular frameworks
- small, simple, and easy to read
- easily separable and deletable **parts**
- no build step needed

## as close to standard JS/HTML as possible

Let's *rely as much as possible on the tools already given.* **Modern HTML, JS, and CSS have recently given us some very powerful tools.**  And since these are non-framework-specific standards, they:
- decrease framework fatigue
- increase the likelihood that people working in our framework will find answers on Stack Overflow
- reduce the amount of code we have to write
- reduce the amount of framework-specific knowledge that users have to learn, making it easier to pick up quickly

## ergonomically similar to popular frameworks

Again, this helps with reducing the learning curve, and prevents having to make huge paradigm shifts in order to start using our framework.

## small, simple, and easy to read

Let's keep things easy to understand and maintain, customize, or copy. Lightweight things can be easier on computing resources, and often have good performance too. Front-end development doesn't have to be as complicated as it is sometimes made to be.

## easily separable and deletable parts

Some framework lock-in is inevitable, but some of it can be avoided. Use the solutions that fit your problem the best, whether that's within this framework or not. A "from-scratch" approach can actually be fun and effective.

## no build step needed

JS is a dynamic language, and it is most enjoyable when it is developed that way. Too many frameworks these days rely on a build or compilation step to get anything done, which slows down development and adds complexity. Sure, a build step might be a good idea when going to production, but for development, good old save and refresh should work like a charm.

Jump to:
 - [custom elements and lifecycle functions](./customElements.md)
 - [data binding](./dataBinding.md)
 - [slots](./slots.md)
 - [automatic registering and cleanup of listeners](./listenerHandling.md)
 - ["memoized-once" functions for computed values](./memoizedOnce.md)
 - [shared state management](./sharedState.md)
 - [Templating](./templating.md)