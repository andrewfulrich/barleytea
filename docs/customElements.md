
# Custom Elements

Custom elements are created just like vanilla js custom elements. 

```javascript
class custom extends HTMLElement {

}
define('custom-el',custom);
```

```html
<custom-el>Hello World</custom-el>
```
[See it in action here](https://jsfiddle.net/n7adoqws/)

## Why use the standard API?

- element lifecycle is handled by the browser - no framework needed (see the "Using the Lifecycle Callbacks" section [of this page](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements))
- put it anywhere, not just under an "#app" node. In things like React, Angular, and Vue, you usually put everything under a **root element**. In the tutorials, it's usually something called "myApp" or it has an id which equals **the word "app",** and if you *happen* to put your custom element *outside of that root,* it doesn't work. With the standard **Custom Elements API,** you can put your tag **anywhere** and expect it to work.
- universal standard - not tied to a framework. See our [Design Goals](./designGoals.md)

## Extending built-in elements

You certainly don't have to, but you might also consider extending built-in elements like so:
```javascript
class myTitle extends HTMLHeadingElement {
  connectedCallback() {
    this.textContent="Hello also!"
  }
}
customElements.define('my-title',myTitle,{extends:'h2'})
```

```html
<h2 is="my-title"></h2>
```
[See it in action here](https://jsfiddle.net/3kpqha6e/)

Some advantages of extending built-in elements:

- Basic display info is provided. Built-ins have their own way of displaying things.
- You don't need to create wrapper elements for the sake of it. 
- Easy a11y (no aria-role camouflage). You don't have to make it look like the built-in because it *is* the built-in
- Progressive enhancement. No javascript? Ok, it can still have basic functionality.
- Use the `[is="my-element"]` css selector to keep your css encapsulated without the need for Shadow DOM

Just be careful: Safari has chosen not to support extended custom built-ins. You'll have to use a [polyfill](https://github.com/ungap/custom-elements-builtin) if you want to do this.

## Under the hood

Within that `define` function, we're taking that class that's passed in, and creating a subclass so that we can inherit all it's functionality while extending it with new functionality. Basically:

```javascript
function define(tagName,theClass,extendsParam=undefined) {
  //...
  class inherited extends theClass {
    render() {
      if(super.render !== undefined && typeof super.render=='function')
        //call the super.render and parse the result to a DOM
    }
    //...
    connectedCallback() {
      if(super.connectedCallback) super.connectedCallback();
      //...
      this.render()
    }
  }
  
  customElements.define(tagName,inherited,extendsParam)
}
```

The above is a little simplified but not much. Notice we can include things like a render function which is now called when the element is connected to the DOM.

For more details on what happens inside render, see the Under the Hood section in [Templating](./templating.md)

Jump to:
 - [design goals](./designGoals.md)
 - [custom elements and lifecycle functions](./customElements.md)
 - [data binding](./dataBinding.md)
 - [slots](./slots.md)
 - [automatic registering and cleanup of listeners](./listenerHandling.md)
 - ["memoized-once" functions for computed values](./memoizedOnce.md)
 - [shared state management](./sharedState.md)
 - [Templating](./templating.md)