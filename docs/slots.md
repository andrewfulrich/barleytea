# Slots

Lots of frameworks use slots despite not using the Shadow DOM (which is a requirement for vanilla js slots). Barleytea.js also does not use Shadow DOM. [Here's a good article about why Shadow DOM may not be a great thing](https://medium.com/@WebReflection/any-holy-grail-for-web-components-c3d4973f3f3f)

## How to use

```javascript
class custom extends HTMLElement {
  render({html,keyed}) {
    return html`
    <div>
      <h3>${this.slots.mySlot}</h3>
      <ul>
        ${this.slots.default
        .map((el,index)=>keyed(index)`<li>${el.textContent}</li>`)}
      </ul>
    </div>`
  }
}
define('custom-el',custom);
```

```html
<custom-el>
  I'm a text node.
  <span slot="mySlot">Named Slot</span>
  <span>Slot with no name</span>
</custom-el>
```
[See it in action here](https://jsfiddle.net/gnupdz47/1/)

Notice you can use named slots or unnamed slots, and you can access them with `this.slots`. The unnamed ones are all in an array under `this.slots.default`.

## Under the Hood

If you remember from [Custom Elements](./customElements.md), we're wrapping customElements.define and extending the passed-in class:

```javascript
function define(tagName,theClass,extendsParam=undefined) {
  //...
  class inherited extends theClass {
    render() {
      if(super.render !== undefined && typeof super.render=='function')
        //call the super.render and parse the result to a DOM
    }
    //...
    connectedCallback() {
      if(super.connectedCallback) super.connectedCallback();
      //...
      this.render()
    }
  }
  
  customElements.define(tagName,inherited,extendsParam)
}
```

Now let's add in slots:
```javascript
function define(tagName,theClass,extendsParam=undefined) {
  //...
  class inherited extends theClass {
    render() {
      if(super.render !== undefined && typeof super.render=='function')
        //call the super.render and parse the result to a DOM
    }
    //...
    connectedCallback() {
      if(super.connectedCallback) super.connectedCallback();
      //...

      /*****************Slot Handling *************************/
      if(this.slots == undefined) {
        const newSlots = {default:[]};
        Array.from(this.childNodes).forEach(el => {
          const slotName=el.getAttribute && el.getAttribute('slot')
          slotName? newSlots[slotName] = el : newSlots.default.push(el)
        });
        this.slots=newSlots
      }

      this.render()
    }
  }
  
  customElements.define(tagName,inherited,extendsParam)
}
```

Before the first render, we get all the element's child nodes. Then for each of them, we see whether these are named slots or not, and save them to `this.slots` accordingly. We got slots in about 8 lines of code.

Jump to:
 - [design goals](./designGoals.md)
 - [custom elements and lifecycle functions](./customElements.md)
 - [data binding](./dataBinding.md)
 - [automatic registering and cleanup of listeners](./listenerHandling.md)
 - ["memoized-once" functions for computed values](./memoizedOnce.md)
 - [shared state management](./sharedState.md)
 - [Templating](./templating.md)