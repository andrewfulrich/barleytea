# Change Log

## 0.6.1 - December 22 2022
Simplified the keyed tag array replacement logic

## 0.6.0 - June 18 2022
When passing functions down to children in the template, bind them to original element
Also updated dev dependencies to resolve npm audit issues

## 0.5.0 - September 13 2020
Templating engine bugfix: keyed render arrays weren't clearing all the way

## 0.4.1 - September 13 2020
Templating engine bugfix: adding items to the middle of a keyed render's array was erroring out

## 0.4.0 - September 8 2020
LIFECYCLE METHOD CHANGE: I moved the call `super.connectedCallback` to the bottom of `connectedCallback` in the element so it's called just before render but after all the framework-specific stuff has been applied.

WHY: Previously when a `connectedCallback` was called from the element you made with `define(...)`, it would not be able to access the proxied versions of `this.state`, `this.cached`, etc. and also wouldn't be able to access `this.slots`. Consequently you weren't able to do things like use `this` as the element itself within a cached function. Now, since it's at the bottom of that function instead of the first command, all that stuff has now already been set up so it should be able to work in a more predictable manner.

## 0.3.2 - September 6 2020
Minor templating engine bugfix: for text nodes, when replacing a null with a string value, the value was getting appended instead of replaced

## 0.3.1 - September 4 2020
Added some docs around partial string attributes not being supported, what they are, and what you can do instead.

Added more docs around routing options and the fact that the templating engine's `parse` function can be used directly.

Also added some more friendly errors: 

 - Partial string attributes are not supported, so when someone tries to use them, it now gives an error saying so
 - A common mistake is forgetting to return from the render function, which previously resulted in a somewhat cryptic error about destructuring. Now it is an error that says the render function returned undefined.
 - A common mistake is forgetting to pass a key into the keyed function. The wording of the error that is thrown has been updated to be clearer about the usage.

## 0.3.0 - August 30 2020
Fixed a bug preventing single-letter html elements from getting parsed correctly in the templating engine

## 0.2.0 - August 7 2020
Added `.ref` magic property to the templating engine, see [this link under Props-> Special "ref" Prop](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/customElements.md)

## 0.1.0 - July 22 2020
Initial Release