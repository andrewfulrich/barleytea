# Barleytea.js

Barleytea.js strives to be a transparent, familiar framework by:
 - keeping things as close to vanilla js as possible
 - providing a lot of the functionality you like from js frameworks you've worked with
 - in small(-ish), readable chunks so that you can modify/replace/copy at will
 - with [removable parts](https://andrewfulrich.gitlab.io/barleytea/distroMaker.html) in case you don't need all of it

See our [Design Goals](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/designGoals.md) for more details on what this framework strives for.

It's currently about 3.1 KB minified/gzipped.

## Features

Framework includes:
 - [custom elements and lifecycle functions](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/customElements.md)
 - [data binding](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/dataBinding.md)
 - [slots](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/slots.md)
 - [automatic registering and cleanup of listeners](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/listenerHandling.md)
 - ["memoized-once" functions for computed values](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/memoizedOnce.md)
 - [shared state management](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/sharedState.md)
 - ~~Routing~~ (No routing because [I personally don't prefer to use browser-side routing](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/yUNoRoute.md), but if that's your thing, [it's really easy to write it yourself](https://jsfiddle.net/Lxydmr5a/1/), or maybe consider using [RLite](https://github.com/chrisdavies/rlite))

[Templating](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/templating.md) includes:
 - V-DOM-less DOM diffing
 - xss protection
 - element property setters
 - self-closing tags
 - keyed renders

Also:
 - Use defined elements anywhere, no need to render it within an "app" element
 - Same function signature as `customElements.define` to minimize framework-specific knowledge

Not interested in all of that? [Pick and choose what you want](https://andrewfulrich.gitlab.io/barleytea/distroMaker.html)

## To use:

Here's a customary to-do list with Barleytea.js:

`TodoList.js`
```javascript
class TodoList extends HTMLElement {
  state = {
    todos: ['write tests', 'debug code']
  }

  // this makes it possible to quickly add a todo on an "Enter" keypress
  handleKeyDown = e => {
    if (e.key === 'Enter' && e.target.value) {
      e.preventDefault()
      this.addTodo(e.target.value)
      e.target.value = ''
    }
  }

  // add a new todo with the given string value
  addTodo = todo => {
    this.state.todos = [...this.state.todos, todo]
  }

  // remove a todo by index
  removeTodo = idx => {
    this.state.todos = [...this.state.todos.slice(0, idx), ...this.state.todos.slice(idx + 1)]
  }

  render({ html, keyed, state }) {
    return html`
      <div>
        <h1>To dos:</h1>
        <input 
          placeholder='Enter a new item here' 
          .onkeydown=${this.handleKeyDown}>
        <button 
          .onclick=${this.addTodo}>Add</button>
        <ul>
          ${state.todos.map((todo, idx) => keyed(idx)`
            <li>
              ${todo} 
              <button
                .onclick=${() => this.removeTodo(idx)}>X</button>
            </li>`
          )}
        </ul>
      </div>
    `
  }
}

define('todo-list', TodoList)
```
`index.html`
```html
<html>
  <script src="barleytea.js"></script>
  <script src="TodoList.js"></script>
  <body>
    <todo-list />
  </body>
</html>
```
[See it in action here](https://jsfiddle.net/3ndbjvx9/1/)

We're using [the vanilla.js way to define a custom element](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements) but instead of using `customElements.define`, we're using [our own define function](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/customElements.md). Within the class definition, we have some magic attributes:
 - state is for props with [data-binding](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/dataBinding.md)
 - cached is for [functions that only re-run when their arguments change](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/memoizedOnce.md)
 - render will re-render the element (only repaints what changed- see [templating](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/templating.md))
 - listeners defines all [auto-added listeners](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/listenerHandling.md), and the `window` object within it defines listeners to attach to the window
 - if you have any [slot elements](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/slots.md) within your class, you can find them in `this.slots` (slots.default holds all unnamed slots, while, for example, slots[mySlotName] will hold the slot named "mySlotName")
 - for shared state, we have a `createStore` function [you can read more about here](https://gitlab.com/andrewfulrich/barleytea/-/blob/master/docs/sharedState.md)

## To Download

You can download it from [here](https://andrewfulrich.gitlab.io/barleytea/distroMaker.html) for now, or get the whole thing from [/public/barleytea.js](./public/barleytea.js) in this repo.

## Performance

Seems to run [DBMonster](https://andrewfulrich.gitlab.io/barleytea/test/dbMonster/DBMonster-barleytea.html) pretty quickly, even slightly faster than React, on my machine at least. You can read more about that [here](./test/dbMonster/readme.md)

## Tests

The template code tests can be run [here]((https://andrewfulrich.gitlab.io/barleytea/test/SpecRunner.html)

There's a lot of code examples in the docs that demonstrate each part of the framework.

To run the tests locally (as well as distromaker etc.), you can `npm install` and then `npm run local`

## Contribution Steps

Here are some general (not always applicable) steps for contributing back changes to the UI.

1. Make the change in the code. If it's in the templating, change `public/parts/templating.js`. If it's in the element framework, change `public/parts/framework.js`.
2. Run distroMaker and tests locally with `npm install; npm run local` and go to `http://localhost:3000/public/distroMaker.html` and `http://localhost:3000/public/test/SpecRunner.html`
3. Make a test for the change
4. Copy the distroMaker output over to barleytea.js and barleytea-common.js
5. If it's a new feature, make a new section in the docs explaining the new feature and usage, along with a working code example.
6. Make a Merge Request